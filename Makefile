.DEFAULT_GOAL := help
BUILD_DIR = $(PWD) # for subdirs
IN_DIR := ./src
OUT_DIR := ./out
OUT_NAME := handy-sicherheit
IN_FILES := $(IN_DIR)/*.md
TEMPLATE_FILE := templates/default.tex
IMAGES := $(shell find ./img -type f)
META := metadata.yml

PANDOC_CMD := pandoc $(IN_FILES) --fail-if-warnings --pdf-engine=xelatex --metadata-file=$(META) --from=markdown+grid_tables --template=$(TEMPLATE_FILE)

.PHONY: help
help: ## Print the help message
	@awk 'BEGIN {FS = ":.*?## "} /^[0-9a-zA-Z_-]+:.*?## / {printf "\033[36m%s\033[0m : %s\n", $$1, $$2}' $(MAKEFILE_LIST) | \
		sort | \
		column -s ':' -t

$(OUT_DIR):
	mkdir -p $(OUT_DIR)

PDF := $(OUT_DIR)/$(OUT_NAME).pdf
$(PDF): Makefile $(OUT_DIR) $(META) $(IN_FILES) $(IMAGES) $(TEMPLATE_FILE)
	$(PANDOC_CMD) -o $(PDF)

.PHONY: pdf
pdf: $(PDF) ## Make the pdf

PDF_IMPOSED := $(OUT_DIR)/$(OUT_NAME)_imposed.pdf
.PHONY: pdf-imposed
pdf-imposed: $(PDF_IMPOSED).pdf ## Create the imposed PDF

.PHONY: $(PDF_IMPOSED).pdf
$(PDF_IMPOSED).pdf: $(PDF)
	pdfbook2 -n -p a4paper -s $(PDF) && \
		mv $(OUT_DIR)/$(OUT_NAME)-book.pdf $(PDF_IMPOSED)

.PHONY: word-count
word-count: ## Count the words
	$(PANDOC_CMD) -t plain -o - | wc -w

.PHONY: clean
clean:
	rm -rf $(OUT_DIR)

OPEN=$(word 1, $(wildcard /usr/bin/xdg-open /usr/bin/open))

.PHONY: open
open: $(PDF) ## Open the PDF
	$(OPEN) $(PDF)
