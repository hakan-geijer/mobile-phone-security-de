# Mobile Phone Security for Activists and Agitators

A German translation of a comprehensive zine on mobile phone security with an emphasis on thread modeling and trade offs of various technologies and practices.

## (Anti-)Copyright

This work is in the public domain under a CC0 license.
