<!-- vim: set spelllang=de : -->

# Anmerkung des Übersetzers

Übersetzungen sind nie exakt, und sie müssen lokalisiert werden.
Es werden nicht nur die Wörter, sondern auch die Inhalte übersetzt, was zu Unterschieden zum Originaltext führt.
Ich habe spezifisch Anmerkungen für die BRD hinzugefügt und für den englischen Sprachraum relevant Anmerkungen entfernt.
Da in der deutschen Sprache viele Anglizismen verwendet werden, habe ich in diesen Fällen beide Begriffe mit aufgenommen.

Vorwärts!

--- M.
