<!-- vim: set spelllang=de : -->

# Einführung

Einige der mächtigsten Werkzeuge, die uns zur Verfügung stehen, sind unsere mit dem Internet verbundenen Smartphones.
Die sofortige Kommunikation und die Summe des gesamten menschlichen Wissens, das zum Greifen nah ist, erhöht unsere Fähigkeit, die Welt um uns herum enorm zu beeinflussen.
Doch diese Konnektivität hat ihren Preis in der zunehmenden Überwachung durch staatliche Sicherheitsapparate und Privatpersonen.
Diejenigen, die in radikalen/linken Bewegungen aktiv sind, sind sich---in unterschiedlichem Ausmaß---dieser Überwachung bewusst.
Gemeinsam haben wir Praktiken der operativen Sicherheit (manchmal Betriebssicherheit, als OpSec abgekürzt) und eine interne Sicherheitskultur entwickelt, um Störungen unserer Bemühungen, uns zu organisieren, entgegenzuwirken.

Es existieren viele Mythen über die Nutzung von Handys.
Sie beruhen auf Missverständnis über deren Technologien und über die Fähigkeiten, die Staat und privat Akteuren verfügen, um Privatpersonen zu überwachen.
Bei der Erstellung von Bedrohungsmodellen geht es darum, Bedrohungen zu erkennen und spezifische und pragmatische Gegenmaßnahmen zu entwickel.
Doch ohne genaue Modelle deiner Gegner:innen führen solche Modelle zu unwirksamen Gegenmaßnahmen.
Maßnahmen, die auf der Grundlage von Fehlinformationen ergriffen werden, können zu einer leichten Verhaftung führen oder den Eindruck von allwissenden Gegner:innen erwecken, was wiederum Aktionen behindert.
Diese Zine befasst sich mit den grundlegenden Technologien von Handys und geht auf verbreitete Mythen ein, damit du und deine Genoss:innen Störungen widerstehen und sich effektiv organisieren können.

So etwas wie perfekte Sicherheit gibt es nicht.
Es handelt sich nicht um ein Binär, die entweder „an“ oder „aus“ ist, und auch nicht um ein Spektrum von „besserer Sicherheit“ oder „schlechterer Sicherheit“.
Sicherheit wird am besten als „sicherer unter diesen Bedingungen gegen diese Bedrohungen“ diskutiert.
Was vielleicht wirksam ist, um den Staat davon abzuhalten, deinen Standort über dein Handy zu verfolgen, kann nutzlos sein, um eine:n missbräuchliche:n Partner:in davon abzuhalten, deine Nachrichten zu lesen.
Diese Zine soll dir helfen, die Risiken zu verstehen, denen du ausgesetzt bist, damit du fundierte Entscheidungen treffen kannst.
Sicherheitskultur ist keine Garantie für Sicherheit, aber sie ist Schadensbegrenzung.
Du kannst deine Inhaftierung verhindern oder dein Leben oder das Leben der Menschen in deinem Umfeld retten.

Diese Zine wurde Anfang 2022 von Anarchist:innen aus Europa und Nordamerika (und original auf Englisch) geschrieben, und daher wird dieses Wissen für diejenigen, die uns räumlich und zeitlich nahe stehen, am relevantesten sein.
Wir lassen absichtlich (die meisten) rechtlichen Überlegungen weg.
Nur weil deine Gegner:innen etwas nicht tun dürfen, heißt das nicht, dass sie es nicht trotzdem tun werden.
Stattdessen konzentrieren wir uns darauf, was technisch möglich ist.
Wir erkennen auch die Vorurteile an, über die wir nicht hinwegsehen können (sie finden sich immer noch in dieser Zine), und wir sind nicht in der Lage, die Zukunft vorherzusagen.
Du musst das Wissen über deinen persönlichen und lokalen Kontext nutzen, um das hier Geschriebene an die spezifischen Bedrohungen anzupassen, denen du ausgesetzt bist.
