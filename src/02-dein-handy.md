<!-- vim: set spelllang=de : -->

# Dein Handy und du

Dein Handy ist nicht nur ein wertvoller persönlicher Besitz.
Es ist eine Erweiterung deiner Persönlichkeit.
Es enthält deine Erinnerungen, dein Wissen, deine privaten und halbprivaten Gedanken.
Es ermöglicht dir, schnell Informationen nachzuschlagen und sie mit anderen zu teilen.
Diese Konnektivität und der Zugang zu Wissen machen uns bei der Verfolgung unserer Ziele effektiver.
Handys sind---bis zu einem gewissen Grad---auch zu einer Voraussetzung für das Funktionieren in der modernen Gesellschaft geworden.
Aus diesem Grund sind wir selten ohne sie unterwegs.
Wenn der Akku leer ist oder wir ohne es das Haus verlassen, fühlen wir uns nackt, unfähig oder als würde ein Teil von uns fehlen.

Das Eindringen in ein Handy---entweder durch Beschlagnahmung oder durch Malware---durch eine:n Gegner:in kann verheerende Folgen haben.
Alle deine Fotos, Textnachrichten, E-Mails und Notizen könnten Gegner:innen zugänglich gemacht werden.
Sie könnten Zugriff auf alle derzeit angemeldeten Konten auf deinem Handy haben.
Installierte Malware oder Stalking-Apps könnten das Mikrofon deines Handys oder die Echtzeit-Ortung aktivieren, nachdem es dir zurückgegeben wurde.

Abgesehen von diesen Arten der aktiven Überwachung bietet dein Handy privilegierten Parteien wie der Polizei, die Massen- oder Echtzeitzugriff auf Metadaten anfordern kann, die deinem Netzbetreiber oder Internetdienstanbieter zur Verfügung stehen, eine passive Überwachung.

Aufgrund dieser Überwachungsmöglichkeiten sagen Aktivist:innen zu Recht: „Your phone is a cop“ und „Your phone is a snitch.“[^Handy-Bulle]
Sollten wir Handys aufgrund ihren Fähigkeiten weiter benutzen, weil sie uns die Möglichkeit dazu geben, oder sollten wir sie wegen der Gefahren, die sie darstellen, ausrangieren?
Oder gibt es vielleicht eine Nuance in der Frage, wann und wie wir Handys benutzen können, die es uns erlaubt, viele ihrer Vorteile zu behalten und gleichzeitig viele ihrer Nachteile zu vermeiden?

[^Handy-Bulle]: *Anm. d. Übersetzers*: Übersetzt als „Dein Handy ist ein Bulle“ und „Dein Handy ist ein:e Spitzel:in,“ aber solche Sprichwörter gibt's leider nicht auf Deutsch.
