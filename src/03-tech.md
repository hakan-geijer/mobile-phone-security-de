<!-- vim: set spelllang=de : -->

# Handy-Technik

Um zu verstehen, wie Handys kompromittiert und zur Überwachung genutzt werden können, müssen wir genau wissen, wie die verschiedenen in Handys verwendeten Technologien funktionieren, z. B. die Hardware, die Firmware und das Betriebssystem des Handys, die Mobilfunknetze und bis zu einem gewissen Grad auch das Internet insgesamt.
Das wird dir helfen, ein Bedrohungsmodell zu erstellen, damit du fundierte Entscheidungen treffen kannst, was dem Auswendiglernen scheinbar willkürlicher Maßnahmen vorzuziehen ist.

## Mobilfunknetze

Mobilfunknetze bestehen aus überlappenden Zellen, die von Transceiver-Sendemasten versorgt werden.
In städtischen Gebieten ist die Netzabdeckung dichter, so dass ein einzelnes Handy mit mehr Sendemasten in Kontakt steht.
In Vorstädten und ländlichen Gebieten gibt es weniger Überschneidungen, so dass ein Handy mit weniger Sendemasten in Kontakt steht.

Provider können Informationen über das Signal selbst verwenden, um den Standort des Handys zu bestimmen.
Der grobe Standort kann anhand des Einfallswinkels am Sendemast oder anhand des Sektors[^Sektor], aus dem das Signal kommt, bestimmt werden.
Wenn die Entfernung eines Handys von mehreren Masten gleichzeitig gemessen wird, kann der Provider den Standort des Handys sehr genau triangulieren.[^Multilateralisierung]
LTE-Netze können die Position eines Handys durchschnittlich 25 Meter genau bestimmen, und 5G-Netze können dies bis auf 5 Meter genau tun.
Je mehr Sendemasten es gibt, desto genauer kann der Standort des Handys bestimmt werden.
Daher ist die Triangulation auf dem Land im Allgemeinen weniger genau als in der Stadt.

[^Sektor]: Die kegelförmige Fläche, die von einer einzelnen Antenne abgedeckt wird.
[^Multilateralisierung]: Das heißt „Aufwärtsstrecke-Multilateralisierung.“
Übrigens verwenden wir „Triangulation“ im Sinne von „Multilateralisierung,“ weil es sich in diesem Fall lohnt, technische Genauigkeit gegen Verständlichkeit einzutauschen.

Wenn Handys eine Verbindung zu einem Mobilfunknetz herstellen, senden sie eine eindeutige Geräte-ID (IMEI[^IMEI]) zusammen mit ihrer Teilnehmer-ID (IMSI[^IMSI]).
Eine IMSI wird normalerweise auf einer physischen SIM-Karte[^SIM] oder einer eSIM[^eSIM] gespeichert.
Dies bedeutet, dass der Austausch mehrerer SIM-Karten zwischen einem Gerät oder einer SIM-Karte zwischen mehreren Geräten eine feste Verbindung zwischen diesen Identitäten herstellen kann.
Eine gültige SIM-Karte oder IMSI ist nicht erforderlich, um einen Anruf zu tätigen.
Sie dient lediglich der Authentifizierung des Geräts gegenüber dem Provider und bestimmt, ob das Gerät Anrufe tätigen oder mobile Daten nutzen darf.
In den meisten (wenn auch nicht allen) Regionen können beispielsweise Notrufe auch ohne SIM-Karte getätigt werden.
Das Entfernen einer SIM-Karte aus deinem Handy **verhindert nicht** die Ortung.

[^IMEI]: Internationale Mobilfunk Ausstattungs-Identität (*International Mobile Equipment Identity*).
[^IMSI]: Internationale Mobilfunk Teilnehmerkennung (*International Mobile Subscriber Identity*).
[^SIM]: Teilnehmer-Identitätsmodul (*Subscriber Identity Module*).
[^eSIM]: Embedded-SIM, ein direkt in das Gerät integrierter Chip.

## Bauarten

Die meisten Menschen, die „Handy“ sagen, meinen ein „Smartphone“, d. h. ein Gerät mit einem Betriebssystem und Apps, die von den Benutzer:innen installiert werden können.
Ein „klassisches“ Handy ist die am wenigsten ausgefeilte Art von Mobiltelefon, wie sie in den frühen Tagen der weit verbreiteten Nutzung von Mobiltelefonen üblich war und mit der mensch nur telefonieren und SMS-Nachrichten versenden konnte.
Heutzutage sind Featurephones eher selten.
Sie liegen irgendwo zwischen Smartphones und klassischen Handys.
Sie können über herstellerspezifische Apps wie einen E-Mail-Client oder einen Webbrowser verfügen.
Um Featurephones und klassisches Handys zusammen von Smartphones zu unterscheiden, wird der Begriff Tastenhandys[^Tastenhandys] für die beiden erstgenannten Bauarten verwendet.

[^Tastenhandys]: *Anm. d. Übersetzers*:
Die ursprünglichen Autor:innen verwendeten den Ausdruck *simple phones* („einfache Handys“), um Tastenhandys zu beschreiben.
Im Deutschen beschreibt Tastenhandys die äußeren Merkmale und nicht die Funktionen, daher möchte ich hier klarstellen, dass die Verwendung des Begriffs Tastenhandy in diesem Zine „nicht-Smartphone“ bedeutet und nicht unbedingt, dass es Tasten hat.

### Smartphones

Smartphones verfügen in der Regel über einen Ortungsdienst, mit dem das Handy hochpräzise Echtzeit-Standortdaten für Apps, insbesondere Karten, bereitstellen kann.
Der Ortungsdienst nutzt die von GPS-[^GPS] oder GLONASS-Satelliten[^GLONASS] empfangenen Signale, um die Position des Handys zu triangulieren.
Die meisten Handys verwenden A-GPS[^A-GPS], das empfangene Sendemasten-Signale, WiFi-Signale und sogar über das Internet ausgetauschte Daten kombiniert, um die Position des Handys schneller und genauer zu berechnen.

[^GPS]: Globales Positionsbestimmungssystem (*Global Positioning System*), von dem Verteidigungsministerium der Vereinigten Staaten betrieben.
[^GLONASS]: Globales Satellitennavigationssystem, eine von der russischen Roscosmos betriebene Alternativ zu GPS.
[^A-GPS]: Unterstütztes GPS (*Assisted GPS*).

Smartphones enthalten oft auch einen Kompass, einen Beschleunigungsmesser, ein Gyroskop und ein Barometer.
Auch ohne GPS oder Multilateralisierung können die Messungen dieser Sensoren kombiniert werden, um den aktuellen Standort aus einer bekannten früheren Standort abzuleiten.

Das bedeutet, dass GPS-Signale zwar passiv von einem Gerät empfangen werden, die Verwendung von Ortungsdiensten jedoch den Standort des Handys übermitteln kann und dass das Deaktivieren von Ortungsdiensten möglicherweise nicht ausreicht, um zu verhindern, dass eine App oder Malware auf deinem Handy deinen Standort grob bestimmt.

### Tastenhandys

Viele Aktivist:innen glauben, dass die Verwendung von Tastenhandys anstelle von Smartphones „sicherer“ ist.
Da ein Handy ohne GPS oder Ortungsdienst immer noch geortet werden kann, bieten Tastenhandys keinen nennenswerten Schutz vor Standortverfolgung.
Bei Featurephones gibt es in der Regel keine weit verbreiteten Text- oder Voice-Chat-Apps, und Tastenhandys verfügen definitionsgemäß nicht über solche Funktionen.
Das bedeutet, dass nur unverschlüsselte SMS und Telefongespräche zur Verfügung stehen, die in höherem Maße abhörgefährdet sind, als wenn sie über eine Client-Server- oder End-zu-End-Verschlüsselung verfügen.
Tastenhandys, die technologisch am wenigsten fortschrittlich zu sein scheinen, verfügen möglicherweise nur über 2G-Funktionen, was bedeutet, dass Anrufe und SMS mit Geräten im Wert von nur etwa 25 Euro abgehört werden können.
Darüber hinaus können viele dieser Geräte über versteckte Internetfunktionen verfügen, die Telemetriedaten an die Hersteller zurücksenden, ohne dass die Nutzer:innen dies bemerken.

Einfach ausgedrückt: Tastenhandys sind gegen die meisten Bedrohungen, denen die meisten Aktivist:innen ausgesetzt sind, **nicht sicherer als Smartphones.**

## Malware 

Malware (ein Schadprogramm) ist bösartiges (*malicious*) Software. 
Es handelt sich um ein Programm, das etwas tut, was du nicht willst, und versucht, seine Aktivitäten zu verbergen.
Vom Staat geschaffene Malware[^Staatstrojaner] hat oft das Ziel, andere Handys oder sogar elektronische Geräte wie WiFi-Router zu überwachen und sich zu verbreiten.

[^Staatstrojaner]: *Anm. d. Übersetzers*: Oft wird der Begriff „Staatstrojaner“ für alle vom Staat geschaffene Malware benutzt.

Alte Internet-Sicherheitstrainings besagen, dass Malware durch den Besuch fragwürdiger Websites oder das Öffnen von E-Mail-Anhängen von unbekannten Empfängern installiert wird, und obwohl dies immer noch stimmt, ist die Angriffsfläche deines Handys viel größer.
Die meisten, wenn nicht sogar alle, deiner Apps fragen nach Mitteillungen oder warten auf Mitteillungen von z. B. Google Play Services und stellen dann Anfragen an die Server der App.
Einige Malware sind Zero-Click, d. h. sie erfordern keine Benutzerinteraktion.
Die Spyware Pegasus der NSO Group beispielsweise nutzte einen Zero-Click-Exploit und zielte auf Aktivist:innen, Journalist:innen und Politiker:innen ab.
Malware kann auf unsere Handys installiert wird, selbst wenn du nur vertrauenswürdige Apps verwenden und nur (wissentlich) Nachrichten von vertrauenswürdigen Kontakten akzeptierst.

Manche Malware verbleibt nur im Arbeitsspeicher eines Handys, solange das Handy eingeschaltet ist, und ist nicht in der Lage, über Neustarts hinweg zu bestehen.
Aus diesem Grund missbraucht manche Malware die Abschaltroutine des Handys und führt eine falsche Abschaltung durch.
Dennoch kann ein regelmäßiger Neustart des Handys Malware beseitigen.

Wenn du glaubst, dass dein Handy kompromittiert wurde, musst du Malware-Spezialist:innen aufsuchen, die dir bei die Feststellung helfen, und du musst dich möglicherweise ein neues Gerät zulegen.
Malware kommt seltener vor, als du denkst, aber lass dich durch dieses Seltenheit nicht dazu verleiten, legitime Warnzeichen zu ignorieren.
Sogenannte Staatstrojaner lassen sich nicht so leicht erkennen wie einfach Malware, so dass die üblichen Methoden möglicherweise nicht anwendbar sind.
Du kannst sie leider nicht selbst erkennen.

## Betriebssysteme

Eine der häufigsten Fragen, die Aktivist:innen zu Smartphones stellen, lautet: „Was ist sicherer, iOS oder Android?“
Wie bei allen Sicherheitsfragen lautet die Antwort „Es kommt darauf an.“

Smartphone-Betriebssysteme (*Operating System*, OS) gibt es in zwei Varianten: iOS für Apple-Geräte und Android für alle anderen.
iOS ist proprietär mit privatem Quellcode.
Android ist ein Basisbetriebssystem mit öffentlichem Quellcode (*Open-Source*), den die Hersteller für ihre Geräte ändern können.
Die Android-Betriebssysteme der Hersteller haben in der Regel einen privaten Quellcode.
Darüber hinaus gibt es viele Vollversionen von Android, die von der Open-Source-Community entwickelt werden, insbesondere LineageOS,[^cyanogen] GrapheneOS und CalyxOS sind Open-Source Android-Betriebssysteme, bei denen Datenschutz und Sicherheit im Vordergrund stehen.

[^cyanogen]: LineageOS ist der Nachfolger der beliebten, aber eingestellten CyanogenMod.

Wenn ein Handy eingeschaltet wird, beginnt die Hardware mit dem Laden des Betriebssystems unter Verwendung eines Prozesses, bei dem jeder Schritt die Integrität der für den nächsten Schritt benötigten Software überprüft.
Es gibt verschiedene Bezeichnungen dafür, z. B. *Secure Boot* (sicheres Boot[^Bootstrap]) oder *Verified Boot* (verifiziertes Boot).
Um ein benutzerdefiniertes Betriebssystem zu installieren, muss dieser verifizierte Boot-Prozess deaktiviert werden, da sich die Hardware sonst weigern würde, das benutzerdefinierte Betriebssystem zu laden, da es nicht durch einen vertrauenswürdigen kryptografischen Schlüssel, der vom Originalhersteller mitgeliefert wurde, signiert ist.
Dadurch besteht die Möglichkeit, dass anstelle des echten Betriebssystems ein bösartiges Betriebssystem installiert wird, das deine Daten lesen kann, entweder durch physischen Zugriff oder durch Malware.
Das bedeutet jedoch nicht, dass Standard-Betriebssysteme mehr oder weniger sicherer sind als benutzerdefinierte Betriebssysteme.
Es bedeutet, dass die Deaktivierung des verifizierten Boots und die Verwendung eines benutzerdefinierten Betriebssystems ein anderes Risikoprofil aufweisen.

[^Bootstrap]: *Anm. d. Übersetzers*: *Boot* ist die Abkürzung für *Bootstrap* (Stiefelriemen).
Es ist ein Begriff für etwas, das von selbst beginnt (ungefähr).

Wenn Malware entwickelt wird, zielt sie auf einzelne Geräte oder ein einzelnes Betriebssystem ab.
Die Entwicklung von Malware ist kostspielig und zeitaufwändig, und wenn die Malware erst einmal installiert ist, kann es durch Updates der App oder des Betriebssystems entdeckt und daran gehindert werden, neue Geräte zu infizieren.[^Malware-Wiederverwendung]
Aus diesem Grund ist es wirtschaftlicher, Malware zu schreiben, die viele Benutzer:innen angreifen kann.
Für iOS gibt es eine begrenzte Anzahl von Versionen für eine begrenzte Anzahl von Geräten, während das Android-Ökosystem sehr viel vielfältiger ist.
Das bedeutet, dass es für Gegener:inner weniger wirtschaftlich und schwieriger ist, Android-Nutzer ins Visier zu nehmen.

[^Malware-Wiederverwendung]: Darüber hinaus hat Malware die interessante Eigenschaft, dass sie, wenn sie eingesetzt wird, eingefangen und geklont werden kann, so dass andere sie wiederverwenden können.
Das wäre so, als ob jedes Mal, wenn eine Rakete auf feindlichem Gebiet landet, die Chance bestünde, dass sie sofort kopiert und unendlich vervielfältigt werden könnte, und außerdem wäre es sehr viel wahrscheinlicher, dass dieser bestimmte Raketentyp in Zukunft abgefangen wird.
Die Militärs würden zögern, so viele Raketen abzufeuern, und müssten ihre Ziele viel strategischer wählen.

Unsere Empfehlungen lauten wie folgt:

- Für die meisten Personen, die versuchen, Massenüberwachung und weniger motivierte Hacker:innen zu vermeiden, sind iOS oder normales Android ausreichend, da sie am einfachsten zu benutzen sind.
- Personen, die sich stark in sozialen Bewegungen engagieren oder damit rechnen, dass sie individuell überwacht werden, empfehlen wir zum jetzigen Zeitpunkt für ihre organisatorische und politische Arbeit die Verwendung von GrapheneOS ohne Google Play Services, die Verwendung von f-droid als einziges App-Repository und die Installation nur der minimalen Anzahl von Apps, die für die Kommunikation erforderlich sind.
- Für Personen, die die Aufmerksamkeit von Geheimdiensten auf sich gezogen haben oder erwarten, dies zu tun, sollten Handys für alles, was mit Aktivismus zu tun hat, vermieden werden.

## Geräteverschlüsslung

iOS und Android bieten die Möglichkeit, deine persönlichen Daten zu verschlüsseln.
Dies geschieht unter verschiedenen Namen wie *Data Protection* (Daten-Verteidigung) oder *Device Encryption* (Geräteverschlüsselung).
Bei Handys ist die Geräteverschlüsselung in der Regel **nicht** standardmäßig aktiviert.
Diese Funktion **muss** vom Benutzer:innen entweder beim Einrichten des Handys oder später in den Einstellungen aktiviert werden.
Ebenso muss der Schutz vor übermäßigen Anmeldeversuchen aktiviert werden.

Geräteverschlüsselungsimplementierungen verwenden in der Regel ein Hardware-Sicherheitsmodul (*Hardware Security Module*, HSM) oder einen Sicherheits-Coprozessor,[^Sicherheits-Coprozessor] spezielle Chips im Handy, die die Ver- und Entschlüsselung sowie die für diese Vorgänge verwendeten kryptografischen Schlüssel verwalten.
Diese Chips sind wichtig, weil sie die Schlüssel vor unbefugtem Zugriff und Manipulationen schützen.
Diese Chips können den Zugriff von Gegner:innen auf deine Daten verhindern, aber das ist keine Garantie.
Das Tool GrayKey---neben anderen---ist in der Lage, Sicherheitslücken in HSMs auszunutzen, und in einigen Fällen kann es das Entsperrungspasswort schnell knacken und die Daten entschlüsseln.
In HSMs, die heute vielleicht noch sicher sind, könnten nächsten Monat neue Lücken entdeckt werden, und die Strafverfolgungsbehörden könnten in fünf oder zehn Jahren neue Techniken zur Wiederherstellung von Daten entwickeln.
Die Geräteverschlüsselung leistet gute Arbeit, wenn es darum geht, den Zugriff auf deine Daten zu verhindern, wenn ein Einbrecher Zugang zu deinem Handy erhält oder wenn Bullen es bei einer Durchsuchung mitnehmen.
Es ist unwahrscheinlich, dass sie den konzertierten Bemühungen staatlicher Geheimdienste wie dem BND, dem MI5 oder dem FBI, auf deine Daten zuzugreifen, standhält.

[^Sicherheits-Coprozessor]: Bei Apple-Geräten wird dieser Chip *Secure Enclave* (sichere Enklave) genannt.

Ein bekanntes Beispiel dafür ist, dass das FBI das Passwort des Handys des Täters etwa ein Jahr nach dem 2015 Amoklauf in San Bernardino, Kalifornien geknackt hat.
Etwa fünf Jahre später wurde aufgedeckt, dass der Zugriff auf die Daten über eine Kette von Sicherheitslücken gegen die Software im HSM erfolgte.

Die Verwendung von Geräteverschlüsselung kann zum Schutz vor Datenerfassung beitragen, aber **der einzige Weg, um sicherzustellen, dass die Daten nicht in die Hände der Strafverfolgungsbehörden gelangen, ist, dass diese Daten gar nicht erst existieren.**

## VPNs

Ein virtuelles privates Netzwerk (*Virtual Private Network*, VPN) bezeichnet in dem von den meisten Aktivist:innen verwendeten Kontext eine App, die den Internetverkehr eines Geräts an einen Dienst weiterleitet, dessen Zweck es ist, den Webverkehr und die IP-Adresse des Benutzers vor Netzwerkbeobachtern oder den Servern, mit denen eine Verbindung hergestellt wird, zu verschleiern.
VPNs schützen deinen Datenverkehr vor dem Schnüffeln in öffentlichen WiFi-Netzwerken und verbergen deine IP-Adresse vor Servern, mit denen du dich verbindest.
Du kannst Ermittlungen in die Irre führen und die passive Überwachung erschweren, aber VPN-Apps können Datenverkehr durchlassen, oder du vergisst, sie zu aktivieren.
Der Verkehr zu und von deinem VPN-Anbieter kann von staatlichen Geheimdiensten, die den gesamten Internetverkehr einsehen können, korreliert werden, und dein VPN kann rechtlich dazu gezwungen werden, Protokolle zu sammeln oder an die Strafverfolgungsbehörden zu übergeben.
VPNs sind billig und können die Sicherheit in gewisser Weise verbessern, aber mensch sollte sich nicht darauf verlassen, dass sie Anonymität gegenüber dem Staat bieten.

## IMSI-Catcher

Ein IMSI-Catcher[^StingRay] (Fänger) ist ein Gerät, das einen legitimen Sendemast vortäuscht und Handys dazu bringt, sich mit ihm zu verbinden, um so das Abhören oder das Senden von SMS-Nachrichten oder Anrufen zu ermöglichen.
Manchmal ist dieses Spoofing (Verschleierung/Vortäuschung) nachweisbar, aber darauf solltest du dich nicht verlassen.
In einigen Regionen kann die Polizei ohne Durchsuchungsbefehl eingesetzt werden, insbesondere bei Demos.
IMSI-Catcher funktionieren zum Teil durch Herabstufung des Protokolls auf ein unverschlüsseltes oder unverschlüsselbares Protokoll.
Obwohl Smartphones Protokolle bevorzugen, die einen besseren Schutz gegen Abhören und Spoofing bieten, damit die Handys auch in Regionen mit nur 2G funktionieren, und weil dies Teil des GSM-Standards ist, können Smartphones von IMSI-Catchern dennoch auf unsichere Protokolle heruntergestuft werden.
Von Smartphones gesendete und empfangene Anrufe und SMS-Nachrichten sind nicht gegen das Abfangen durch IMSI-Catcher geschützt.

[^StingRay]: Im Englischen werden die IMSI-Catcher oft unter dem populären Markennamen *StingRay* geführt.
*Anm. d. Übersetzers*: In der BRD werden sie in radikalen Kreisen kaum bzw. gar nicht diskutiert, also habe ich keine Ahnung wie/ob mensch sie nennt.

## Faraday-Taschen

Handys senden und empfangen Informationen per elektromagnetischer Strahlung.
Diese Strahlung kann durch spezielle Materialien blockiert werden.
Mythen und einige unterstützende Belege besagen, dass Signale blockiert werden können, indem mensch ein Handy in eine oder mehrere Kartoffelchips-Tüten mit Folienauskleidung steckt, aber darauf---wie auf viele andere Gegenmaßnahmen---sollte mensch sich nicht verlassen.
Es kann eine speziell angefertigte Faraday-Tasche erworben werden, mit der sich Telefonsignale zuverlässig blockieren lassen.

Wenn du Handys transportierst und sicherstellen musst, dass sie keine Signale durchlassen, reicht es möglicherweise nicht aus, sie auszuschalten.
Bei den wenigsten Smartphones können die Batterien entfernt werden.
Wenn ein Gegenstand in der Tasche auf das Handy drückt, kann die Einschalttaste betätigt werden.
Malware kann die Abschaltroutine manipulieren und verhindern, dass das Handy tatsächlich ausgeschaltet wird, wenn du versuchst, es auszuschalten.
Wenn du ein ausgeschaltetes Handy in einen Faraday-Tasche steckst, kannst du verhindern, dass es Signale sendet, und die Wahrscheinlichkeit, dass sein Standort ermittelt werden kann, wird erheblich verringert.
