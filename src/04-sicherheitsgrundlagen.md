<!-- vim: set spelllang=de : -->

# Sicherheitsgrundlagen

Es gibt einige Handypraktiken, die für die meisten Aktivist:innen ratsam sind.
Einige davon werden hier beschrieben.

## Softwareaktualisierungen

Zweifellos, das Beste, was du tun kannst, um zu verhindern, dass du von den Strafverfolgungsbehörden---oder zufälligen Hacker:innen---gehackt wirst, ist die unverzügliche Installation von Updates für das Betriebssystem deines Handys und allen Apps.
Es mag lästig sein, aber viele Updates enthalten Sicherheits-Updates für kritische Schwachstellen.
Dies kann zumindest verhindern, dass deine Bank-, Geldtransfer- oder Zahlungskonten geleert werden.

## Passwort-Managers

Die zweite nützliche und allgemein anwendbare Sicherheitspraxis ist die Verwendung eines Passwort-Managers (Passwortverwaltungs) für alle deine Konten, einschließlich der auf deinem Handy verwendeten.
Es gibt kostenpflichtige Varianten, die die automatische Synchronisierung von Passwörtern zwischen Geräten und die automatische Anmeldung bei Webseiten ermöglichen.
Diese erfordern jedoch ein bisschen Vertrauen in das Unternehmen, das das Produkt anbietet.
Es gibt kostenlose Alternativen wie KeePassX, die jedoch nicht besonders benutzerfreundlich sind wie die kostenpflichtigen Produkte.
Wenn du einen Passwort-Manager verwendest, sollten alle deine Konten sichere, eindeutige und zufällige Passwörter haben.
Diese werden in der Regel automatisch von dem Manager generiert.
Das Passwort zum Entsperren des Managers sollte eine lange, zufällige Phrase sein.

Menschen sind notorisch schlecht darin, die für Passwörter benötigte Zufälligkeit zu erzeugen, und die Anfangszeile deines Lieblingsgedichts oder einige ausgeklügelte Substitutionsregeln, um `antifaschismus` in `an7if4sc1hsmu5` zu ändern, können von Computern ziemlich leicht geknackt werden.
Diceware ist eine Methode zur Erstellung von Passwörtern durch Würfeln und die Auswahl von Wörtern aus einer vordefinierten Liste.
Fünf Wörter sind das absolute Minimum, sechs sind besser, aber alles über acht ist übermäßig.
Auf diese Weise erhält mensch eine unberechenbare Zufälligkeit, die mensch nicht selbst erzeugen kann, und die mensch sich zudem leicht merken kann.
Eine benutzerfreundliche Wortliste in englischer Sprache wird von der EFF bereitgestellt.
Die gebräuchlichste deutschsprachige Wortliste von A. G. Reinhold ist eher umständlich, und stattdessen wird die Liste von dys2p empfohlen.[^dys2p-Wortliste]^,^[^dys2p-Anmerkung]
Eine Beispielphrase ist `EbensoBatzenTackerBrandSeegang` (bitte **nicht** tatsächlich _diese_ Phrase verwenden; mach dir eine eigenen).

[^dys2p-Wortliste]: [https://github.com/dys2p/wordlists-de](https://github.com/dys2p/wordlists-de)
[^dys2p-Anmerkung]: *Anm. d. Übersetzers*: Ich habe diesen Satz im Rahmen der Lokalisierung des Textes in den Absatz eingefügt.
Im Original kam er natürlich nicht vor.

: Auszug aus den Wortlisten

+----------+---------------+---------------+
| **Zahl** | **EFF**       | **dys2p**     |
+==========+===============+===============+
| `24311`  | drowsily      | erdreich      |
+----------+---------------+---------------+
| `24312`  | drudge        | erdrosselt    |
+----------+---------------+---------------+
| `24313`  | drum          | erdrutsch     |
+----------+---------------+---------------+
| `24314`  | dry           | erdteil       |
+----------+---------------+---------------+
| `24315`  | dubbed        | erdtrabanten  |
+----------+---------------+---------------+
| `24316`  | dubiously     | erdulden      |
+----------+---------------+---------------+
| `24321`  | duchess       | erdumlaufbahn |
+----------+---------------+---------------+
| `24322`  | duckbill      | erdwall       |
+----------+---------------+---------------+

## Sperren des Handys

Je nach Bedrohungsmodell solltest du die Entsperrung deines Handys erschweren oder nahezu unmöglich machen.
Dies ist besonders wichtig, da die Entsperrmethode auch die Entschlüsselungsmethode ist.
Eine starke Entsperrmethode schützt also vor unerwünschtem Zugriff auf deine Daten, wenn dein Handy gekapert wird.
Im Allgemeinen solltest du Passwörter den PINs oder Mustern vorziehen, da erstere für Maschinen schwieriger zu knacken sind.
Du solltest auf jeden Fall die Entsperrfunktion für das Gesicht deaktivieren und eventuell auch die Entsperrung per Fingerabdruck ausschalten.
In einigen Regionen sind Passwörter rechtlich geschützt, nicht aber Fingerabdrücke oder andere biometrische Merkmale.

Einige Handys bieten die Möglichkeit, alle Daten zu löschen, wenn es zu viele falsche Entsperrungversuche gibt.
Du solltest diese Funktion aktivieren (und das Handy dann von neugierigen Kleinkindern und Haustieren fernhalten).

Deaktiviere Mitteillungen auf dem Sperrbildschirm oder zumindest sie von Apps, die sensible Informationen enthalten könnten.
Deaktiviere den Zugriff auf Apps von deinem Sperrbildschirm.

Wenn du die Geräteverschlüsselung auf deinem Handy aktiviert hast, sind deine Daten am stärksten geschützt, wenn dein Handy ausgeschaltet ist (oder eingeschaltet wurde, aber dein Entsperrungspasswort noch nicht eingegeben wurde).
Nachdem du dein Handy einmal entsperrt haben, sind deine Daten weniger geschützt.

Viele Aktivist:innen lassen die Fingerabdruck-Entsperrung aktiviert, weil sie im Vergleich zur eines Passwort mit 30 oder mehr Zeichen 100 mal Eingeben pro Tag enorm bequem ist.
Da das Bedürfnis nach Bequemlichkeit oft über das Bedürfnis nach besserer Sicherheit siegt, ist dies ein weiterer Grund, keine sensiblen Informationen auf deinem Handy zu speichern.
Wenn du die Fingerabdrucksperre aktiviert hast, kannst du sie vorübergehend deaktivieren, indem du die Einschalttaste gedrückt hältst.
Du kannst dies tun, bevor du mit Strafverfolgungsbehörden interagieren, ins Bett gehen oder dein Handy unbeaufsichtigt lassen.

## Drahtlose Funktionen

Du solltest WiFi und Bluetooth deaktivieren, wenn du sie nicht verwendest.
Beide können für das *Fingerprinting* (Fingerabdrucken) und die Identifizierung deines Handys verwendet werden.
Außerdem vergrößern sie die Angriffsfläche für Hacker:innen, die versuchen, in dein Handy einzudringen.
Auch wenn die Risiken, die mit der ständigen Aktivierung dieser Funktionen verbunden sind, minimal sind, können diese Praktiken deine Sicherheit geringfügig verbessern, und wenn du sie nicht brauchst, warum solltest du es nicht tun?

## Datensicherung

Smartphones verfügen oft über eine Funktion für automatische Backups in einem mit dem Handy verbundenen Cloud-Konto (bei Apple für iOS und bei Google für Android).
Apple hat seine Pläne für verschlüsselte Backups in der iCloud auf Druck des FBI gestoppt, und seine Backups sind unverschlüsselt.
Google bietet Ende-zu-Ende-verschlüsselte Backups an, die nach externen Überprüfungen starke Datenschutzgarantien von Google selbst oder den Strafverfolgungsbehörden bieten.
Darüber hinaus können einige Apps ihre eigenen Backupdienste anbieten.
WhatsApp zum Beispiel kann deine Unterhaltungen auf seinen Servern sichern.

Wir empfehlen, Backups bei Apple zu vermeiden, aber Backups bei Google sind sicher genug, da du ohnehin keine belastenden Beweise auf deinem Handy haben solltest.
Data, die an Dritte gesendet werden, können jederzeit verloren gehen oder zerstört werden.
Um die Daten zu sichern solltest du darüber nachdenken, sei auf einer verschlüsselten externen Festplatte zu speichern und diese zu Hause oder an einem sicheren Ort aufbewahren.
Damit die Polizei die Daten nicht wiederherstellen kann, sollte die Festplatte verschlüsselt werden.

## Chat-Apps 

Text- und Sprachnachrichten-Apps bieten sicherere Alternativen zu Telefonanrufen und SMS-Nachrichten.

### Verschlüsselung

Chat-Apps bieten eine von zwei Arten der Verschlüsselung.

Bei der **Client-Server-Verschlüsselung** wird der Kanal zwischen einem Client (z. B. deinem Handy) und dem Server verschlüsselt und vor Abhören oder Manipulationen geschützt.
Die Nachricht wird entschlüsselt und auf dem Server gespeichert.
Wenn die Nachricht von einem anderen Client (z. B. dem Handy einer befreundete Person) angefordert wird, wird sie für die Übertragung erneut verschlüsselt und gesendet.

Bei der **Ende-zu-Ende-Verschlüsselung (E2EE)** erzeugen die Clients kryptografische Schlüssel und tauschen ihre öffentlichen Teile untereinander aus.
Die Nachrichten werden mit dem öffentlichen Schlüssel des anderen Clients verschlüsselt und über den Server gesendet, wobei der Server nur als unwissendes Relais fungiert, da die Nachrichten nur von dem anderen Client entschlüsselt werden können.

E2EE bedeutet lediglich, dass ein Server oder eine andere Person, die sich zwischen deinem Handy und dem Handy deines Gesprächspartners befindet, eine Nachricht nicht lesen oder verfälschen kann.
Ein:e Gegner:in kann aus den Metadaten Informationen über die Größe der Nachricht, den Zeitpunkt des Versands, den Absender und den Empfänger ableiten.

Einige Chat-Apps bieten nur optionale E2EE an, wie z. B. Telegram mit ihren geheimen Chats, aber diese Funktion ist nicht für Gruppen verfügbar.
Bei anderen Apps wie Signal oder Wire ist E2EE obligatorisch, ebenso wie bei iMessage (Apple) und WhatsApp.[^p2p-chat]
Bei einigen Apps wie Element ist E2EE standardmäßig aktiviert, kann aber aus Kompatibilitätsgründen mit älteren Clients deaktiviert werden.

[^p2p-chat]: Es gibt interessantere *Peer-to-Peer-* (Rechner-Rechner-Verbindung-) Chat-Apps wie Briar und Cwtch, die Metadaten verheimlichen und andere interessante Sicherheitseigenschaften aufweisen, aber sie haben keine große Verbreitung.
Sie sind auch nicht für iOS verfügbar, was die meisten Besatzungen daran hindert, sie für sichere Kommunikation zu nutzen.

Die Sicherheit von E2EE hängt von der Verifizierung der ausgetauschten Schlüssel ab, die häufig durch das Scannen von QR-Codes erfolgt, die einen Fingerabdruck enthalten, der (statistisch) eindeutig dem generierten Schlüssel zugeordnet ist.
Bei einigen Apps musst du nur einen Fingerabdruck für alle Geräte verifizieren, bei anderen musst du einen Fingerabdruck für jedes Gerät verifizieren.
Einige Apps senden Mitteillungen in der Konversation, wenn sich der Fingerabdruck deines Kontakts ändert, was möglicherweise auf etwas Schändliches hindeutet.
Einige Apps tun dies leider nicht.
Du **musst** alle Fingerabdrücke für alle Geräte verifizieren, und wenn sich ein Fingerabdruck ändert, **musst du ihn erneut verifizieren**, da sonst deine gesamte Sicherheit zunichte gemacht werden könnte.
Außerdem geben einige Chat-Apps die von deinen verifizierten Geräte nicht für andere Geräte frei, und aufgrund dieser schlechten Benutzerfreundlichkeit musst du die Geräte aller deiner Kontakte von jedem deiner eigenen Geräte aus verifizieren.

![QR Code und Fingerabdruck](./img/svg/qr-code.svg){width=80%}

### Nutzung

Das Mantra „Benutzt einfach Signal“ wird von Aktivist:innen oft wiederholt, aber es geht fälschlicherweise davon aus, dass alle Menschen identische Bedrohungsmodelle haben.
In einigen Regionen kann die Nutzung von Signal durch nationale Firewalls blockiert sein, oder die Nutzung ist so selten, dass sie einen Nutzer als verdächtig einstufen könnte.
In Nordamerika und Europa gibt es diese Nachteile im Allgemeinen nicht.
Es gibt jedoch allgemeine Beschwerden gegen Signal, z. B. dass eine Telefonnummer für die Registrierung erforderlich ist und dass Kontaktlisten nur auf halbwegs sichere Weise mit dem Server geteilt werden, um die Kontaktsuche und den anfänglichen Schlüsselaustausch zu ermöglichen.

Bei den meisten Chat-Apps werden die Nachrichten beim Empfang entschlüsselt und im Klartext auf dem Gerät gespeichert.
Bei einigen Chat-Apps wie Signal kannst du ein Passwort festlegen, um den Zugriff auf die Nachricht zu verhindern, während eine andere Person deiner Handy benutzt, aber dies verschlüsselt sie in keiner Weise erneut.
Wenn die Geräteverschlüsselung auf deinem Gerät aktiviert ist, erhältst du einen gewissen Schutz für diese Nachrichten, wie im Abschnitt über die Geräteverschlüsselung beschrieben.

Da Nachrichten im Klartext gespeichert werden und selbst bei Geräteverschlüsselung wiederhergestellt werden können, solltest du die Funktion zum Verschwindenlassen von Nachrichten aktivieren.
Bei einigen Chat-Apps kann ein:e Teilnehmer:in die Funktion „Nachrichten verschwinden lassen“ für alle Teilnehmer:innen des Chats aktivieren.
Bei anderen Chat-Apps muss jede:r Teilnehmer:in die Option zum Verschwindenlassen von Nachrichten aktivieren, um sicherzustellen, dass alle Nachrichten schließlich verschwinden.
Es kann unpraktisch sein, verschwindende Nachrichten zu haben, da die Suche nach einem Bild, einer Datei oder einer Entscheidung nur bis zu einer Woche oder einem Monat zurück möglich ist.
Dies ist vielleicht besser als ein mehrere Jahre langes Protokoll von allem, was mensch gesagt oder gedacht hat, und vor allem von allen Orten, an denen mensch angeblich gewesen ist.

Das bedeutet, dass du Chat-Apps mit obligatorischem E2EE bevorzugen solltest, es sei denn, es gibt einen zwingenden Sicherheitsgrund, der dagegen spricht.

### Nicht „einfach Signal benutzen“

Verschiedene Datenschutz-Organisationen und besorgte Aktivist:innen haben hervorragende Arbeit geleistet, um die Akzeptanz von Signal in der breiten Öffentlichkeit und insbesondere bei Aktivist:innen zu fördern.
Möglicherweise haben sie das _zu_ gut gemacht, denn viele Menschen haben das so verstanden, dass „wenn mensch Signal benutzt, dann ist sie:er total sicher.“
Das hat dazu geführt, dass einige Leute Dinge besprechen, die sie auf keinen Fall über elektronische Medien besprechen sollten, und dann davon ausgehen, dass es in Ordnung ist, weil sie Signal benutzen.
Jede Sicherheitsmaßnahme geht von einer Reihe von Annahmen aus, und davon ausgehend kann es akzeptierte Risiken oder Dinge geben, die außerhalb des Anwendungsbereichs liegen.
Signal ist sehr gut darin, einen staatlichen Akteur daran zu hindern, eine Massenüberwachung zu nutzen, um den Inhalt von Textnachrichten zu lesen.
Es verbirgt sogar einige---aber nicht alle---Metadaten.
Andere Chat-Apps haben ein ähnliches Bedrohungsmodell.
Wenn dein Handy jedoch durch Malware kompromittiert wird, weil du die Aufmerksamkeit auf sich gezogen hast oder einfach nur Pech hattest, wird Signal nicht verhindern, dass deine Nachrichten gelesen werden.[^Infiltrator:innen] 

[^Infiltrator:innen]: Darüber hinaus gibt es grausame Sicherheitspraktiken, wie z. B. die Teilnahme an vielen großen Signal-Gruppenchats und die Diskussion ihrer Aktionen, ohne zu überprüfen, wer sonst noch in der Gruppe ist.
Es spielt keine Rolle, wie gut die Verschlüsselung ist, wenn eines der Gruppenmitglieder ein:e Infiltrator:in oder Spitzel:in ist.

![Eingabemethode und Pinyin Optionen](./img/bin/ime-pinyin-optionen.png){width=50%}

Bei einigen Sprachen, insbesondere bei Sprachen, die auf Zeichen statt auf Buchstaben basieren, wird ein Eingabemethoden-Editor (*Input Method Editor*, IME) verwendet, um Sequenzen von lateinischen Buchstaben in die Zeichen der Zielsprache zu konvertieren.
Dabei handelt es sich häufig um installierte Apps von Drittanbietern.
Signal warnt Nutzer, die IMEs verwenden, nicht ausreichend vor der Möglichkeit, dass ihre Chats von der Software gelesen und an den Staat gemeldet werden könnten, bevor die Nachrichten verschlüsselt werden.

**Signal ist keine Garantie für Sicherheit**.
Das Gleiche gilt für jede andere E2EE-Chat-Apps.
Behandel sie nicht als eine.

Auch wenn wir hier starke Kritik an Signal üben, ist diese Kritik auf die Popularität von Signal und die falschen Vorstellungen zurückzuführen.
Zum Zeitpunkt des Verfassens dieses Artikels ist Signal immer noch eine der wenigen verschlüsselten Chat-Apps, auf das mensch sich für die Sicherheit verlassen kann.

*Anm. d. Übersetzers*:
Da dieser Text für das Vereinigte Königreich und die USA geschrieben wurde, wäre es nachlässig, wenn ich hier keine speziellen Bemerkungen zur BRD hinzufügen würde.
In der BRD (zu oft) stößt Signal auf merkwürdigen Widerstand von Aktivist:innen.
Chat-Apps mit schwächeren Sicherheitseigenschaften wie Telegram oder XMPP werden oft bevorzugt, und früher erfreute sich Threema einer gewissen Beliebtheit.
Ein Teil davon scheint aus einem grundsätzlichen Misstrauen gegenüber den USA, den Amis und ihrer Technik zu kommen.
Kettennachrichten mit dubiosen Behauptungen, wie Signal sei gehackt oder kaputt, machen oft die Runde, und es wird wenig bis gar nicht nachgefasst oder korrigiert.
Wenn du glaubst, dass Telegram oder ein selbst-konfiguriertes XMPP-System sicherer ist als die meisten der Bedrohungen, denen wir hier ausgesetzt sind, dann hast du dich gewaltig verrechnet.
Überdenk es noch einmal.

## E-Mail

Es gibt Möglichkeiten, die E-Mail-Kommunikation sicherer zu machen, aber E-Mail als Protokoll und Kommunikationsmedium ist generell nicht sicher für private Kommunikation.
Boutique- und aktivistenfreundliche E-Mail-Anbieter (d. h. Nicht-Gmail/Nicht-Microsoft/etc.) bieten keine signifikanten Sicherheitsvorteile gegen das Abfangen durch Strafverfolgungsbehörden oder Hacker:innen.
Beim Versenden von E-Mails verwenden einige Leute PGP oder S/MIME, aber diese sind schwierig zu benutzen und bieten insgesamt eine schlechte Benutzererfahrung.
Zwei Personen, die diese Verschlüsselungsmethoden verwenden, können einen recht guten Schutz gegen das Mitlesen ihrer E-Mails haben, aber ein einziger falscher Klick kann den gesamten Verlauf einer Konversation im Klartext senden, so dass er für die Strafverfolgungsbehörden einsehbar wird.
ProtonMail hat kühne Behauptungen über die Verschlüsselung ihrer E-Mails und Clients aufgestellt, und viele Aktivist:innen haben diese Halbwahrheiten dahingehend interpretiert, dass die Nutzung eines ProtonMail-Kontos bedeutet, dass _alle_ ihrer E-Mails verschlüsselt sind, was jedoch nicht der Fall ist.
E-Mail sollte generell für die Planung und insbesondere für die sichere Kommunikation vermieden werden.

Dennoch ist die E-Mail nach wie vor beliebt, weil jedes Gerät E-Mails senden und empfangen kann und manche Leute „keine Chat-Apps“ nutzen.
Für die Koordinierung einer örtlichen Mietervereinigung oder die Einrichtung von Schichten im örtlichen Infoshop ist E-Mail vielleicht ganz gut geeignet.
Wenn du sich für die Verwendung von E-Mail entscheidest, geh davon aus, dass die Strafverfolgungsbehörden alle Nachrichten lesen, und beschränke die Konversation auf ein Minimum.
Besprich keine illegalen Aktivitäten.
Besprich keine Drama der Szene, die vom Staat ausgenutzt werden können.

Schließlich gibt es legitime Anwendungsfälle, in denen E-Mail und PGP ein letzter Ausweg sein können, z. B. ein einmalig verwendbarer verschlüsselter Kanal für jemanden, der auf der Flucht ist, damit er einen zweiten, sichereren Kanal einrichten kann.
In solchen Fällen sollten Handys immer noch vermieden werden, da sie sich leicht aufspüren lassen.

## Mehrere Identitäten, Mehrere Handys

Je nach deinem Bedrohungsmodell kannst du dich dafür entscheiden, mehrere Handys zu unterhalten, die mit deinen verschiedenen Identitäten mit verschiedenen Decknamen verbunden sind.
Du kannst zum Beispiel ein Handy haben, das mit deinem öffentlichen Leben verbunden ist, mit Konten in den sozialen Medien, die du nutzt, um mit deiner Familie in Kontakt zu treten, und ein zweites Handy mit einer separaten SIM-Karte und separaten Konten, die mit deinem Leben als Aktivist:in verbunden ist.
Diese Trennung der Konten ist Teil eines Prozesses, der Kompartmentalisierung genannt wird.

Der erste Vorteil besteht darin, dass die Verwendung unterschiedlicher Geräte für jedes deiner Identitäten verhindert, dass Programmier- oder Benutzerfehler diene privaten Informationen preisgeben.
Apps auf deinem Handy können ein unerwartetes Verhalten zeigen, z. B. indem sie deiner gesamten Kontaktliste eine Verbindungsanfrage senden, wenn du dich bei einer neuen Chat-App anmeldeset.
Du kannst einen Fehler machen und auf einen Post in sozialen Medien vom falschen Konto aus antworten.
Wenn du auf eine E-Mail-Adresse klickst, um einen deiner Identitäten zu verwenden, beginnt das Betriebssystem deines Handys möglicherweise, eine E-Mail mit einem Standard-E-Mail-Client zu verfassen, der mit einem anderen Identität verbunden ist.

Der zweite Vorteil ist, dass dein aktivistisches Gerät auf ein Minimum reduziert werden kann und nur für sichere Kommunikation verwendet wird.
Jede App, die du installierst, ist ein möglicher Weg für Malware, auf dein Handy zu gelangen.
Wenn dein Handy also nur ein einfaches Betriebssystem und zwei Chat-Apps hat, ist es schwieriger zu kompromittieren.

Die Verwendung mehrerer Handys allein hindert die Strafverfolgungsbehörden nicht daran, deine Identitäten miteinander zu verknüpfen.
Wenn du die Handys zur gleichen Zeit bei sich tragen oder an den gleichen Orten benutzen, können sie miteinander verbunden werden.

Als Alternative zu mehreren Handys kannst du das Risiko von Datenverlusten durch Fehler oder unerwartetes Verhalten verringern, indem du mehrere Profile auf deinem Android-Gerät erstellst.
Dies schützt dich zwar nicht vor Malware, aber es bietet einen gewissen Schutz.

Einer der häufigsten Anwendungsfälle für mehrere Handys ist das Organisieren einer Gewerkschaft.
Einige Unternehmen verlangen die Installation von Apps für die Fernverwaltung, um das geistige Eigentum des Unternehmens zu schützen oder um Sicherheitsverletzungen zu verhindern.
Dabei handelt es sich um Spyware-Apps, die dein Handy vollständig kontrollieren können.
Abgesehen davon verlangen viele Unternehmen eine Chat-App für die Kommunikation.
Du solltest es vermeiden, sich auf Geräten des Unternehmens oder auf Geräten mit installierter Spyware zu organisieren, und du solltest den Unternehmens-Chat nicht für gewerkschaftliche Organisierungsbemühungen nutzen.

## Wegwerf-, Demo-, und Einweghandys 

Die meisten Menschen sind sich der Bedeutung ihrer Handys bewusst und wissen, dass sie von ihnen geortet werden können oder dass ihr Verlust verheerend sein kann.
Eine Reihe von Ansätzen wird von Aktivist:innen---und anderen---verwendet, um ihr Risiko zu verringern, auch wenn sie ihr Risiko oder die Gründe für ihre Gegenmaßnahmen nicht vollständig artikulieren können.

Manche Menschen haben Demohandys oder Wegwerfhandys, die sie zu Aktionen mitnehmen oder beim Grenzübertritt benutzen.
Auf diesen Geräten befinden sich nur wenige private Daten, und sie gelten als nicht vertrauenswürdig---wegen der möglichen Installation von Malware---wenn sie von Strafverfolgungsbehörden behandelt werden.
Diese Handys werden nicht zur Anonymität verwendet.
Sie können eine SIM-Karte mit dem normalen Handy ihres:r Benutzer:in teilen und so verwendet werden, dass die Ortung sie mit dem Wohnort ihres:r Benutzer:in in Verbindung bringt.
Demohandys stellen der Polizei weniger Daten und Konten zur Verfügung, wenn sie beschlagnahmt werden.
Ein Demo- oder Wegwerfhandy muss nicht unbedingt ein Tastenhandy sein.
In vielen Fällen handelt es sich um Smartphones, weil sie ihrem:r Benutzer:in Karten und E2EE-Kommunikation ermöglichen.

Aktivist:innen verwenden fälschlicherweise den Begriff „Einweghandy“ (*burner phone*) für Demohandys, Wegwerfhandys oder Tastenhandys.[^Einweghandy]
Ein Einweghandy verdankt seinen Namen der Tatsache, dass es nur zum einmaligen Gebrauch bestimmt ist und danach zerstört wird.
Sie werden erworben, wenn der:die Benutzer:in während einer Aktion, die zu einem massiven und konzertierten staatlichen Einsatz führen wird, eine mobile Kommunikation benötigt.

[^Einweghandy]: Die Leute scheinen den Begriff „Einweghandy“ zu verwenden, weil er mega-illegal und super-kriminell klingt, und nicht, weil sie tatsächlich die Eigenschaften eines Einweghandys beschreiben.

Damit ein Handy als Einweghandy gilt, muss es die folgenden Kriterien erfüllen:

1. Das Handy muss mit Bargeld gekauft werden.[^Einweghandy-kaufen]
1. Die SIM-Karte, die für das Handy verwendet wird, muss mit Bargeld gekauft werden.
1. Das Handy und die SIM-Karte müssen von einem:r Nutzer:in erworben werden, der zu diesem Zeitpunkt keine anderen Handys oder verfolgbaren Geräte bei sich trägt.
1. Das Handy und die SIM-Karte dürfen nur gemeinsam benutzt werden.
1. Das Handy darf niemals an Orte mitgenommen werden, die mit dem:der Benutzer:in verbunden sind, es sei denn, es ist ausgeschaltet und befindet sich in einer Faraday-Tasche.
1. Das Handy darf niemals in der Nähe von nicht Einweghandys oder anderen Geräten benutzt werden, die auf den:die Benutzer:in oder seine:ihre Freund:innen zurückgeführt werden können.
1. Alle Konten auf dem Handy müssen anonym erstellt, nur mit diesem Handy verwendet und dann nie wieder benutzt werden.
1. Das Handy muss für genau eine Aktion verwendet werden.
1. Das Handy darf nur mit anderen Einweghandys oder unbeteiligten Parteien Kontakt aufnehmen (z. B. mit einem Büro oder einem:r Gegner:in, der:die das Ziel der Aktion ist).
1. Das Handy und die SIM-Karte müssen nach der Aktion ausgeschaltet und sofort vernichtet werden.

[^Einweghandy-kaufen]: Der Klau von Handys mit aktivierter SIM-Karte ist im Allgemeinen nicht zu empfehlen, da bei jedem Klau ein zusätzlicher Standortdatenpunkt erzeugt wird, der mit der Aktion in Verbindung gebracht werden kann, die Handys möglicherweise nicht entsperrt werden können und der:die Besitzer:in die Geräte möglicherweise in die von den Provider geführten Sperrlisten aufnehmen lassen, so dass sie nicht zum Telefonieren oder zur Datennutzung verwendet werden können.

Erschwerend kommt hinzu, dass manche Handys oder SIM-Karten eine Aktivierung erfordern, entweder durch einen Anruf bei einer Nummer oder durch den Besuch der Website des Providers.
Manchmal blockieren diese Websites Verbindungen aus dem Tor-Netzwerk.
Die Verwendung eines nicht Einweghandys, um die SIM-Karte zu aktivieren, ist eine offensichtliche Verletzung der erforderlichen Sicherheitseigenschaften.
Du musst vielleicht ein Münztelefon finden oder eine fremde Person an einem Bahnhof dazu bringen, dir ihr Handy für ein paar Minuten zu leihen.

Wenn wir sagen, dass ein Einweghandy für eine Aktion verwendet werden kann, meinen wir „eine zeitlich begrenzte Abfolge von Aktivitäten.“
Damit kann eine direkte Aktion gemeint sein, die in nur zwei Stunden durchgeführt wird.
Es kann auch die Planung und Koordination im Monat vor einer Aktion sowie die Aktion selbst bedeuten.

Bei besonders vorsichtiger Verwendung kann eine einzelne geschlossene Bezugsgruppe ihren Satz von Einweghandys für wiederkehrende Aktionen wiederverwenden.
Wenn dies der Fall ist, müssen die Handys all gleichzeitig verwendet werden, damit sich die verschiedenen geschlossenen Kreisen von Einweghandys nicht überschneiden.

Eine nicht obligatorische, aber dringend empfohlene Eigenschaft ist, dass Einweghandys nicht unmittelbar vor einer Aktion gekauft werden sollten.
Dies schafft die zusätzliche Möglichkeit, dass das gespeicherte Sicherheitsmaterial des Kaufs noch zugänglich sein könnte.

Der Versuch, die Existenz des geschlossenen Kreisen zwischen den Handys zu verschleiern, kann dazu beitragen, die Entdeckung der Bezugsgruppe zu verhindern.
Ein Schritt besteht darin, sie nicht alle innerhalb eines kurzen Zeitraums zu aktivieren.
Eine schrittweise Aktivierung ist bei der Analyse der Daten durch den Staat weniger auffällig.
Tätige einige Telefonanrufe von zufälligen Standorten aus an Nummern, die jemand plausibelerweise anrufen würde, aber **sprich nicht**, wenn jemand abhebt.
Rufe Nummern an, bei denen lange Wartezeiten zu erwarten sind, z. B. Banken oder Versicherungsfirmen.
Rufe einige lokale Geschäfte an, bevor sie öffnen oder nachdem sie schließen.
Die gefälschten Anrufe sind möglicherweise unnötig, da viele Nutzer in bestimmten Regionen nie telefonieren und einfach ihren Datentarif für alles nutzen.

Aufgrund der Sorgfalt, mit der ein Einweghandy erworben und verwendet werden muss, ist es höchst unwahrscheinlich, dass es die Mühe wert ist.
Wenn du glaubst, dass deine Aktion ein Wegwerfhandy erfordert, solltest du auf jeden Fall versuchen, einen Weg zu finden, die Aktion ganz ohne Handys durchzuführen.
Um anderen zu verdeutlichen, dass ein Einweghandys diese Eigenschaften haben muss, solltest du den Begriff „Einweghandy“ vermeiden und stattdessen „Demohandy“ oder „Wegwerfhandy“ verwenden.[^Wegwerfhandy-Nutzung]

[^Wegwerfhandy-Nutzung]: *Anm. d. Übersetzers*: Die Begriffe Wegwerfhandy und Einweghandy sind im Deutschen sehr ähnlich, so dass es wahrscheinlich am besten ist, Demohandy in allen Fällen zu verwenden, in denen man weder sein Alltags-Handy noch ein Einweghandy benötigt. 

## Kontrollierter Abstieg

In dieser Zine geht es hauptsächlich um ideale Eigenschaften für die sichere Nutzung von Handys, aber oft sind diese Ideale nicht erreichbar.
Ein Beispiel dafür ist, wenn mensch mit Leuten organisiert, die sich keine Smartphones leisten können.
Die Beschaffung von billigen, Tastenhandys für die Organisation einer Aktion oder sogar für die Koordinierung regelmäßiger Treffen kann einfacher und finanziell überschaubarer sein als das Gleiche mit Smartphones zu tun.
Leider bedeutet das Fehlen von verschlüsselten Sprach- und Chat-Apps eine verstärkte Überwachung deiner Nachrichten.

Um zu verhindern, dass der Staat zu viele Informationen über ihre Aktionen erhält, musst du dich auf menschliche Lösungen statt auf technische Lösungen verlassen.
Eine Vereinbarung, dass du Zeiten und Orte von Treffen immer nur mit einem Minimum an Informationen besprichst, kann die gesammelten Informationen auf ein absolutes Minimum reduzieren.
Ein einfaches Codebuch, in dem gängige Phrasen, die bei der Organisation verwendet werden, durch zufällige, unverfängliche Codephrasen ersetzt werden, kann bei Nachforschungen in die Irre führen, und die Verwendung von Codephrasen kann verhindern, dass automatische Systeme die Behörden alarmieren.

Maßnahmen wie diesem ermöglicht es dir, von einer höheren Sicherheit zu einer geringeren Sicherheit überzugehen, ohne sich vollständig der Überwachung und staatlichen Repression auszusetzen.
Diese Methoden erfordern größere Sorgfalt, sind aber machbar.
