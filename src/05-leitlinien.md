<!-- vim: set spelllang=de : -->

# Einen Plan erstellen

Wir können nicht so tun, als ob wir dein Bedrohungsmodell kennen würden, und wir können nicht auf jede Nuance in jeder Region und Situation eingehen.
Was wir tun können, ist, einige Leitlinien aufzulisten, die allgemein anwendbar sind.
Wenn du sie liest, musst du für dich überlegen, was praktisch ist.
Was kannst du tatsächlich tun?
Und was werden die Menschen in deinem sozialen Umfeld tun?
Dein neuer Plan muss nicht perfekt sein.
Er muss nur besser sein als das, was du jetzt tust.
Wenn dies bedeutet, dass du Kompromisse bei der Sicherheit eingehen musst, damit du weiter organisieren kannst, dann kannst du das tun.
Aber lass dich auch nicht durch die schlechte Sicherheit anderer gefährden.
Finde einen Ausgleich.

Dies ist keineswegs eine vollständige Liste, aber es sind einige Möglichkeiten zur Entwicklung einer persönlichen OpSec und Sicherheitskultur:

- Verwende ein Smartphone, da es gegen die meisten Bedrohungen, denen Aktivisten ausgesetzt sind, sicherer ist als ein Tastenhandy.
- Nimm dein Handy nicht zu Aktivitäten mit, die die Polizei interessieren könnten, und insbesondere nicht zu Demos, bei denen es zu Ausschreitungen kommen könnte.
- Bevorzuge verschlüsselte E2EE-Chat-Apps für die Kommunikation, aktiviere das Verschwindenlassen von Nachrichten und vermeide E-Mails.
- Verwende ein Passwort, um dein Handy zu entsperren, und aktiviere die Geräteverschlüsselung.
- Deaktiviere die Fingerabdruck-Entsperrung deines Handys, bevor du ins Bett gehen oder es unbeaufsichtigt lasst.
- Erstelle regelmäßig Backups von Fotos und anderen Daten auf einer verschlüsselten Festplatte oder USB-Stick und lösche diese von deinem Handy.
- Lösche alte Daten: Direktnachrichten (DMs), Gruppenchats, E-Mails, Kalendereinträge, usw.
- Verlasse Gruppenchats, in denen du nicht anwesend sein musst, und entferne inaktive Teilnehmer:innen aus Gruppenchats.
- Übe, dein Handy zu Hause zu lassen oder es auszuschalten, wenn du Besorgungen machst oder kleine Aktionen durchführst, um dich an die Abwesenheit des Smartphones zu gewöhnen.
- Kläre zu Beginn jedes Plenums, ob elektronische Geräte erlaubt sind oder nicht.
  Wenn nicht, schaltet sie aus, sammelt sie ein und bringt sie aus dem Gesprächsbereich.

Und vor allem:

> **Sende keine Nachrichten oder führe keine Telefonate über hochsensible heikle Angelegenheiten.
> Fotografiere und filme keine belastenden Dinge.
> Erstelle keine Beweise, die gegen dich oder andere verwendet werden können.**

## Ungültig, wo verboten

Was hier geschrieben wurde, und auch der Rest dieser Zine, sind Leitlinien.
Sie treffen vielleicht nicht auf dich zu.[^BRD-Vorbehalt]
Vor allem die digitale Sicherheit kann besonders auffällige Spuren hinterlassen.
Wenn Signal in deiner Region sehr ungebräuchlich ist, könnte seine Verwendung dich zu einer Zielscheibe machen.
VPNs können kriminalisiert werden.
Die Verwendung von der Tor-Netzwerk kann dir einen Besuch der Polizei einbringen.
Das Vorhandensein von Apps für sichere Kommunikation auf deinem Handy könnte deine Verhaftung in ein Verschwinden verwandeln.
Bevor du etwas herunterlädst, recherchiere über die Repression in deiner Region, um festzustellen, ob die von uns bereitgestellten Leitlinien dich sicherer machen oder ob sie dich gefährden.

[^BRD-Vorbehalt]: *Anm. d. Übersetzers*:
Da du dies auf Deutsch liest, trifft der Absatz wahrscheinlich nicht auf dich zu, wenn du in der BRD, Österreich oder der Schweiz wohnst.
Er kann jedoch zutreffen, wenn du dies in deiner Muttersprache lesen und in einer anderen Region wohnst.
Ich sage das, weil ich befürchte, dass bei dem derzeitigen Stand der Sicherheitskultur hier und der extremen Paranoia jemand, der diesen Absatz liest, zu dem Schluss kommen könnte, dass diese ganze Zine nicht für sie:ihn und ihre:seine Genoss:innen gilt.

## Alternativen

Es ist immer einfacher zu sagen „Mach mal das stattdessen“ als „Tu das nicht,“ und wenn mensch versucht, ein Verhalten oder eine Praxis zu ändern, erhöht das Anbieten von Alternativen die Chancen, dass jemand das alte, unsichere Verhalten aufgibt.
Es gibt legitime Gründe, Handys zu haben, und Alternativen können eine geringere Belastung bedeuten, wenn wir unsere Handys aufgeben oder unsere Gewohnheiten ändern.

Ein Hindernis für die Vermeidung von Handys ist, dass die Menschen Informationen haben, Informationen sammeln und Kontaktdaten austauschen wollen.
Mit einem Stift und einem Notizblock kannst du die Protokolle deiner Gruppe auf analoge Weise schreiben und später verteilen.
Wenn du geschickt bist, kannst du eine Kopie des kryptografischen Fingerabdrucks (Sicherheitsnummer, *Safety Number*) deines Geräts mit dir führen, um eine sichere Verbindung herzustellen, auch wenn du und dein:e Gesprächspartner:in ihre Handys nicht dabei haben.
Mit einem Papierkalender kannst du Termine planen.
Das Ausdrucken von Papierkarten des Einsatzgebietes einer Aktion kann dir die Orientierung erleichtern.
Wenn du Kopien von Informationen auf Papier erstellst, sorge dafür, dass diese umgehend und sicher entsorgt werden, um zu vermeiden, dass eine buchstäbliche Papierspur deiner Aktivitäten entsteht.

## Handylose Kontingente

Dein Plan könnte zwar heute funktionieren, aber er muss auch zukunftsorientiert sein.
Du kannst dich bei der Organisation stark auf dein Handy verlassen und dabei die Sicherheitsrisiken in Kauf nehmen, aber es könnte eine Zeit kommen, in der Repressionen oder Katastrophen deine Handys oder das Internet lahm legen.
Bei verstärkter Repression ist es üblich, dass der Staat den Mobilfunk- oder Internetzugang für ganze Regionen unterbricht.
Wenn deine Fähigkeit, dich zu organisieren, und deine Sicherheit davon abhängen, dass fast alle Menschen über Handy und ein funktionierendes Internet verfügen, bist du auf bestimmte Arten des Scheiterns gefasst.
Mund-zu-Mund-Propaganda und das sogenannte Turnschuhnetzwerk (*Sneakernet*) sind Ausweichmöglichkeiten, und deine Planung muss die Möglichkeit einbeziehen, dass dies die einzige Möglichkeit ist, Informationen verbreiten.
