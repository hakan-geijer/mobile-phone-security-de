<!-- vim: set spelllang=de : -->

# Fallstudien

Um die vorangegangenen Diskussionen zu konkretisieren, stellen wir eine Reihe von Fallstudien aus unseren Erfahrungen zur Verfügung.
Einige dieser Fälle zeigen Personen, die bereits über genauere Bedrohungsmodelle verfügen, und andere, die dies nicht tun.
Einige beruhen auf Mythen, andere eher auf überprüfbaren Fakten oder sehr wahrscheinlichen Vermutungen.
Wo es Fehler gibt, werden sie diskutiert.

## Fall 1: Plena für eine halb-öffentliche Aktion

### Szenario

Ein Kollektiv plant eine Besetzung, die bis zu ihrem Beginn geheim gehalten und dann über die sozialen Medien bekannt gemacht wird.
Die Planung erfolgt in erster Linie bei Plena in einem örtlichen Kulturzentrum.

### Annahmen

Das Kollektiv geht davon aus, dass die Polizei daran interessiert ist, Besetzungen zu verhindern, und die Aktivist:innen möglicherweise überwacht werden.
Diese Überwachung umfasst unter anderem Staatstrojaner, die sich auf den Handys der Personen befinden könnte.

### Gegenmaßnahmen

Um zu verhindern, dass der Staat die Mikrofone der Handys benutzt, um die Plena aufzuzeichnen, werden die Handys eingesammelt und in einer versiegelten Plastikbox in einem Nebenraum untergebracht.

### Analyse

Es ist richtig, dass die Handys durch Malware kompromittiert worden sein könnten, und es kann richtig sein, dass die Verlegung der Handys in einen anderen Raum die Mikrofone bei der Aufzeichnung der Gespräche behindert.
Dies könnte überprüft werden, indem mensch eine Aufnahme startet, das Handy in die Box legt und dann ein lautes Gespräch führt, um zu sehen, wie viel mensch versteht.
Wenn die Stimmen auch nur annähernd erkennbar sind, könnte mensch mit einer Software zur Audiobearbeitung Ausschnitte davon wiederherstellen.

Wenn das Kulturzentrum nicht regelmäßig nach Mikrofonen oder anderen Abhörwanzen abgesucht wird, können die Gespräche trotzdem aufgezeichnet werden.
Wenn das Kollektiv oder andere Kollektive, die das Kulturzentrum besuchen, stark überwacht werden, könnten in nahe gelegenen Gebäuden platzierte Lasermikrofone die Gespräche aufzeichnen.

Wenn Einzelpersonen passiv überwacht werden, könnte die Tatsache, dass ein Treffen stattgefunden hat und wer daran teilgenommen hat, durch die wiederholte Anwesenheit derselben Gruppe von Handy an einem festen Ort mittwochs von 19 bis 21h über viele aufeinanderfolgende Wochen hinweg aufgedeckt werden.

### Empfehlungen

Wenn Handys eingesammelt werden, um eine Überwachung zu verhindern, sollten sie auch ausgeschaltet werden.
An den Orten, an denen die Handys platziert werden, sollten laute Umgebungsgeräusche herrschen, um die Wahrscheinlichkeit zu minimieren, dass die Handys den Ton der Gespräche auffangen können.

Wenn das Kollektiv glaubt, dass sie wegen Aufruf zu einer Straftat belangt werden könnte, sollte sie die Handys zu Hause lassen oder ausschalten, bevor sie zu den Plena reist.
Dies kann noch weiter minimiert werden, indem mensch nicht dieselben Handy zu der Aktion selbst mitbringt.

Wenn ein hohes Maß an Sicherheit erwünscht ist, kann das Abhören des Raums oder die Aufzeichnung durch staatliche Akteure weiter reduziert werden, indem mensch sich an Orten trifft, die nicht mit Aktivismus verbunden sind.
Wenn sich das Kollektiv der Einfachheit an einem zentralen bekannten Ort treffen will, sollte zu Beginn des Plenums festgelegt werden, dass nur die aktuelle Aktion (und nichts Illegales) besprochen werden soll.

## Fall 2: zufällig mitgehörtes Geschwätz

### Szenario

Einige Mitglieder einer Bezugsgruppe treffen sich in einem Park, um Kontakte zu knüpfen, nicht um eine Aktion zu planen.
Ihre Handys sind dabei und eingeschaltet, aber ihre Sicherheitskultur beinhaltet, dass sie nicht über vergangene Aktionen sprechen oder Kampfsgeschichten austauschen, da diese belastende Informationen enthalten können.

### Annahmen

Die Gruppe geht davon aus, dass die Polizei ihre Gespräche nur abhören will, wenn es um vergangene oder zukünftige illegale Aktivitäten geht.
Sie gehen davon aus, dass ihre Alltagsgespräche uninteressant und uninformative sind.

### Gegenmaßnahmen

Die Gruppe hat gar keine Gegenmaßnahmen ergriffen, um zu verhindern, dass ihre Gespräche abgehört werden.

### Analyse

Wenn die Gruppe bewusst nicht über Pläne oder vergangene Aktionen spricht, dann kann natürlich kein Mikrofon mithören, was nicht laut gesagt wird.
Geplante und durchgeführte Aktionen sind jedoch nicht das einzige, was den Staat interessiert.
Auch Klatsch und Tratsch, Liebesbeziehungen, soziale Bindungen und sogar die Einstellung von Personen und Organisationen innerhalb eines Milieus zueinander sind wertvolle Informationen.
Dadurch kann der Staat genauere soziale Karten erstellen.
Wenn der Staat vermutet, dass eine Person in etwas verwickelt war, das er untersucht, und er weiß, dass diese Person Kompliz:innen hatte, kann die Verwendung von sozialen Karten, die aus zufälligen Gesprächen erstellt wurden, dabei helfen, die Liste der Verdächtig:innen einzugrenzen oder die Mitglieder einer Bezugsgruppe zu ermitteln.
Solche belauschten Gespräche können dem Staat Aufschluss darüber geben, wer sich ausgegrenzt fühlt und Ressentiments hegt, so dass diese Personen gezielt als Spitzel:innen angesprochen werden können.
Kleine Konflikte können ausgenutzt und aufgeheizte Emotionen zu heftigen Auseinandersetzungen angefacht werden.

### Empfehlungen

Unter den Aktivist:innen gibt es einen Generationsunterschied zwischen denjenigen, die sich vor der weit verbreiteten Nutzung von Handys organisiert haben, und denjenigen, die erst nach der Verbreitung von Handys mit der Organisation begonnen haben.
Es gibt auch eine weitere Spaltung zwischen denjenigen, die sich mit Tastenhandys vor der Popularität von Smartphones organisiert haben, und denjenigen, die sich immer in einer Welt organisiert haben, in der fast alle ihre Kontakte Smartphones haben.
Diese Diskrepanz zeigt sich in der Fähigkeit, Pläne in der Annahme zu machen, dass die anderen Personen keine Handys haben, wie z. B. die Festlegung von Orten und Zeiten mit weniger spontanen Änderungen.
Darüber hinaus haben diejenigen, die vor der Einführung von Handys organisiert haben, ein besseres Gespür dafür, wie es war, wenn das Organisieren zunehmend dort stattfand, wo jeder tatsächlich keine Mikrofone dabei hatte.

Wie bereits in dieser Zine erwähnt, ermöglichen uns Smartphones die sofortige Kommunikation und die ständige Verfügbarkeit unbegrenzter Informationen.
Dies hat jedoch den Preis, dass es neue Möglichkeiten der Überwachung gibt.
Aktivist:innen sollten sich darüber im Klaren sein, dass Handys, die sich in Wohnungen, Autos und sozialen Einrichtungen befinden, möglicherweise weiche Informationen über soziale Gruppen sammeln.
Wenn wir empfehlen würden, die Handys häufiger auszuschalten, würden wir vielleicht als Verschwörungstheoretiker:innen oder als unpraktisch belächelt werden.
Die sogenannte liberale Demokratie vermittelt die Illusion, dass wir nicht in einem repressiven Polizeistaat leben, aber es gibt viele Fälle, in denen harmlose soziale Kreise und Aktivistengruppen gehackt und überwacht werden, ganz zu schweigen von den radikaleren und engagierten Gruppen.

Wir schlagen nicht vor, dass wir niemals Handys bei uns tragen sollten, aber wir möchten anregen, dass sich jeder bewusster wird, welchen Aufwand der Staat betreibt, um uns zu überwachen, und wie nützlich die Informationen sind, die wir aus zufälligen Gesprächen gewinnen.
Es könnte eine Zeit kommen, in der die Repression zunimmt und wir ihre Präsenz stärker spüren.
Um uns auf solche Zeiten vorzubereiten und uns Gewohnheiten anzueignen, die uns in die Lage versetzen, einer solchen Repression zu widerstehen, ist unser Vorschlag eher moderat.
Übe dich ab jetzt in erhöhter Sicherheit.
Versuche telefonlose Veranstaltungen zu organisieren.
Wenn ihr zusammen ausgeht oder wandert, auch wenn ihr sich in einer Kneipe trifft, versucht alle dazu zu bringen, eure Handys zu Hause zu lassen.
Gewöhne dich an ihre Abwesenheit.
Spüre die Freiheit zu wissen, dass du keine Standortdaten an den Staat weitergibst und dass niemand außer den Anwesenden deine Gespräche hören kann.

## Fall 3: Besetzen und Tastenhandys 

### Szenario

Ein Kollektiv will ein leerstehendes Gebäude besetzen, um die Aufmerksamkeit auf spekulative Immobilieninvestitionen zu lenken und, wenn die Besetzung erfolgreich ist, das Gebäude in eine kostenlose Unterkunft für die Nachbar:innen umzuwandeln, die kürzlich zwangsgeräumt wurden.
Ein Team wird im Gebäude sein und die Besetzung durchführen, während andere Teams vor Ort mit dem Staat verhandeln und in den sozialen Medien posten werden.

### Annahmen

Die Besetzer:innen sind der Meinung, dass die Polizei ihre Identitäten herausfinden könnte, indem sie sieht, welche Handys innerhalb des Gebäudes kommunizieren, und selbst wenn sie für diese Aktion nicht verhaftet oder strafrechtlich verfolgt werden, könnte dieses Wissen in Zukunft gegen sie verwendet werden.

### Gegenmaßnahmen

Um die Wahrscheinlichkeit zu verringern, dass ihre Identitäten in Erfahrung gebracht wird, falls sie während der Aktion nicht verhaftet werden, hat das Besetzungsteam beschlossen, ihre persönlichen Handys nicht mitzunehmen.
Sie werden nur ein „Einweghandy“ mitbringen, um mit dem Verhandlungsteam zu kommunizieren, damit sie an den Entscheidungen beteiligt werden können, um Beiträge an das Sozialmedien-Team zu senden und um ein Gefühl der Sicherheit zu haben, anstatt bis zum Ende der Aktion isoliert zu sein.
Sie werden ein Handy mit einer SIM-Karte benutzen, die auf keinen ihrer Namen registriert ist, um anonym zu bleiben.

### Analyse

Es ist richtig, dass das Kollektiv keine persönlichen Handys in das Gebäude mitnimmt, in dem sie sich aufhält, da sie damit identifiziert werden könnten.
Die Polizei könnte dies tun, indem sie nachschaut, welche Handys sich im Gebäude befinden und auf wen sie registriert sind oder wo sie die meiste Zeit verbringen (z. B. wenn der:die Benutzer:in zu Hause schläft).
Das Kollektiv bezeichnet das Handy fälschlicherweise als Einweghandy, da seine wiederholte Nutzung dazu verwendet werden kann, es mit dem Kollektiv und Personen in Verbindung zu bringen.
Dieses Handy ist eher als Demohandy zu bezeichnen.
Da ein Teil des Kollektivs ohne Masken außerhalb des Gebäudes bleibt, ist die Identität des Kollektivs bekannt, auch wenn nicht alle Identitäten des Besetzungsteams bekannt sind.
Wenn es sich um ein privates „Einweghandy“ handelt, das eine:r Aktivist:in gehört, und dieses Handy in der Wohnung des:der Aktivist:in eingeschaltet wurde, könnte dies als Beweis dafür dienen, dass der:die Aktivist:in im Gebäude war oder daran beteiligt war.

Das Kollektiv hat die Sicherheitsaspekte bei der Verwendung des Tastenhandys für die Kommunikation mit dem Verhandlungsteam nicht bedacht.
Die Polizei könnte einen IMSI-Catcher einsetzen,[^Besetzung-IMSI] um die SMS-Nachrichten zu lesen, die zwischen dem Besetzungsteam und dem Verhandlungsteam hin und her geschickt werden.
Dies kann der Polizei einen Vorsprung bei den Verhandlungen verschaffen oder ihr die Möglichkeit geben, Uneinigkeit innerhalb des Kollektivs auszunutzen, um eine Räumung leichter zu erzwingen.

[^Besetzung-IMSI]: *Anm. d. Übersetzers*: IMSI-Catcher werden in der BRD selten eingesetzt.
Im Jahr 2017 gab es „nur“ 67 Einsätze, laut Netzpolitik.
Gegenwärtig sind sie von geringer Bedeutung als in andere Länder.
[https://netzpolitik.org/2018/bundesbehoerden-spaehen-immer-oefter-mobiltelefone-aus/](https://netzpolitik.org/2018/bundesbehoerden-spaehen-immer-oefter-mobiltelefone-aus/)

Wenn die Polizei jedoch einen derartigen Aufwand betreibt, um die Personen anhand der bei der Besetzung vorhandenen Handys ausfindig zu machen, ist der Drang nach „Recht und Ordnung“ wahrscheinlich so groß, dass die Besetzung selbst nicht einmal mehr eine praktikable Aktion wäre.

### Empfehlungen

Die Gründe, warum das Besetzungsteam ein einziges Handy mit in das Gebäude bringen wollte, waren legitim, aber sie hätten ein Demohandy mit einem Wegwerfkonto verwenden sollen, das sie in einer E2EE-Chat-App erstellt haben.
Dieses Konto sollte nur mit einem anonymen Konto kommunizieren, das zu den Teams außerhalb des Gebäudes gehört, um zu verhindern, dass das soziale Netzwerk des Kollektivs durchsickert, wenn das Handy beschlagnahmt wird oder die App-Entwickler:innen Daten für diese Konten hätten, die später vor Gericht vorgeladen werden.

## Fall 4: Tastenhandy + Signal-Desktop

### Szenario

Ruben ist ein Aktivist, der sich einem Kollektiv anschließt, von dem er glaubt, dass es wegen seiner regierungsfeindlichen Haltung aktiv überwacht wird.
Um zu verhindern, dass Geheimdienste und die örtliche Polizei ihn verfolgen können, benutzt er ein Tastenhandy mit einer SIM-Karte, wenn er unterwegs ist.
Da einige Gespräche mit seinem Kollektiv sensibler sind, benötigen sie einen verschlüsselten Chat-App und haben sich für Signal entschieden.
Signal erfordert eine Registrierung mit einer Telefonnummer und generiert die ersten kryptographischen Schlüsseln nur bei den iOS- und Android-Apps.
Damit die Signal-Desktop-App auf seinem Laptop funktioniert, hat er die SIM-Karte seines Tastenhandy in das Smartphone seiner Freundin eingesetzt, um ein erstes Schlüsselpaar einzurichten, das er mit seiner Desktop-App verknüpfen konnte.
Anschließend meldet sich Ruben von seinem Konto in der Signal-App auf dem Telefon seiner Freundin ab.

### Annahmen

Rubens Entscheidung, kein Smartphone mit sich zu führen, beruht auf der Überzeugung, dass Smartphones besser auffindbar sind als Tastenhandys.
Ruben geht auch davon aus, dass Signal sicherer ist als Telefonanrufe oder SMS, daher nutzt er Signal für einen Teil seiner Kommunikation.

### Gegenmaßnahmen

Rubens Entscheidung, ein einfaches Telefon zu verwenden, soll die Standortverfolgung über sein Smartphone minimieren.
Seine Entscheidung für Signal-Desktop soll verhindern, dass seine sensiblen Nachrichten an Genoss:innen abgefangen werden.

### Analyse

Rubens Standort ist mit einem Tastenhandy in etwa genauso gut zu orten wie mit einem Smartphone.
Seine Kommunikation ist unsicherer, weil er nicht die Möglichkeit hat, „Notfall“-Nachrichten an Mitglieder seines Kollektivs zu senden oder von ihnen zu empfangen, wenn er sein Tastenhandy benutzt, und wenn er dies tut, werden sie vom Staat abgefangen und gespeichert.
Seine Gegenmaßnahmen gegen die Überwachung haben sowohl für ihn selbst als auch für sein Kollektiv eine Belastung dargestellt, und sie haben ihn nicht wirklich sicherer gegen die Bedrohungen gemacht, denen er ausgesetzt ist.

### Empfehlungen

Ruben sollte sein eigenes Smartphone für die allgemeine Kommunikation verwenden.
Wenn es Zeiten gibt, in denen er seinen Standort verbergen muss oder seine Gespräche nicht abgehört werden sollen, sollte er sein Handy zu Hause lassen.

## Fall 5: Handylose Planung 

### Szenario

Die Mitglieder einer Bezugsgruppe sind schon so lange in den aktivistisch Bewegungen aktiv, dass sie dem Staat bekannt sind.
Sie planen gerade *etwas Großes*.
Sie haben ein Verbot, darüber auf elektronischem Methode zu diskutieren, und besprechen es nur persönlich.

### Annahmen

Sie gehen davon aus, dass der Staat große Anstrengungen unternehmen würde, um ihre Aktion zu verhindern, und noch größere Anstrengungen unternehmen würde, um sie zu untersuchen, nachdem sie stattgefunden hat.
Sie gehen davon aus, dass es möglich ist, dass ihre Elektronik durch staatliche Malware kompromittiert wurde.
Sie gehen davon aus, dass sie selbst bei fehlenden Beweisen auf der Liste der Hauptverdächtigen für die Aktion stehen werden, so dass ihr OpSec für die Aktion hieb- und stichfest sein muss.

### Gegenmaßnahmen

Wegen der Möglichkeit von Malware behandeln sie ihre elektronischen Geräte als nicht besonders vertrauenswürdig.
Wegen der Möglichkeit gezielter Ermittlungen besprechen sie ihre Aktion nicht in ihren Wohnungen, ihren Fahrzeugen oder bekannten Projekte und Räumen, die mit aktivistischen Gruppen verbunden sind.
Um Metadaten, die sie miteinander in Verbindung bringen, zu reduzieren, schalten sie ihre Handys aus, bevor sie an ihren Treffpunkten ankommen, und schalten sie erst wieder ein, wenn sie diese verlassen haben.

### Analyse

Die Bezugsgruppe hat Recht, wenn sie davon ausgeht, dass sie gezielt überwacht wird, und sie hat Recht, wenn sie ihre Handys wie Spitzel behandelt.
Wenn sie ihre Handys ausschalten, verringert sich die Möglichkeit, dass Malware Mikrofone benutzt, um sie auszuspionieren, und es schafft eine gewisse Bestreitbarkeit bezüglich ihrer Standorte während der Plena.
Das Fehlen von Informationen kann jedoch im Vergleich zur normalen Handynutzung ungewöhnlich sein, und wenn alle Handy ungefähr zur gleichen Zeit an einem Ort verschwinden, könnte dies ein Hinweis für den Staat sein, dass während dieser Lücken etwas Bemerkenswertes passiert.
Dies könnte ein Anreiz für zusätzliche Überwachungsmaßnahmen sein, wie z. B. das Abhören des Ortes---wenn sie denselben Ort wiederholt benutzen---oder das Entsenden eines Spions in Zivil, der ein Mikrofon trägt und ihnen in das Café oder die Bar folgt, in dem sie sich treffen.
Wenn ein Mitglied der Bezugsgruppe gefasst wird, aber bei der Vernehmung nichts sagt, könnte die Polizei außerdem die Telefondaten auf Auffälligkeiten überprüfen.
Die Polizei könnte die Daten abfragen, indem sie die entsprechenden Fragen stellt: Welche anderen Handys gingen zu den Zeiten, als dieses Handy ausging, in seiner Nähe aus?
Und was taten die Handys unserer anderen Verdächtigen zu dieser Zeit?
Dies könnte die übrigen Mitglieder der Bezugsgruppe aufdecken oder Beweise dafür liefern, dass die Mitglieder der Bezugsgruppe die Kompliz:innen der Person waren.
Es ist möglich, dass die Polizei nicht daran denkt, diese Fragen zu stellen, oder dass dies nicht zu den Standardmaßnahmen gehört, aber es ist besser, keine Spuren zu hinterlassen.

### Empfehlungen

Da sie mit gezielter Überwachung und Ressourcen zur Untersuchung ihrer Aktivitäten rechnen müssen, sollten sie alle elektronischen Geräte zu Hause lassen und für ihre Plena zufällige Orte wählen, die entweder sehr laut oder sehr abgelegen sind.

## Fall 6: Handys bei Massenaktionen 

### Szenario

Isa ist eine Aktivistin, die vor allem an größeren Demos teilnimmt.
Obwohl sie selbst nicht radikal ist, hat sie einige Freund:innen, die es sind, und sie weiß im Allgemeinen, was sie tun.
Die Faschos haben einen Aufmarsch geplant, und Isa und Freund:innen wollen sich der Menschenmenge anschließen, die ihre geplante Route blockieren will.
Um mit ihren Freund:innen in Kontakt zu bleiben und aktuelle Informationen über die Blockaden oder die Änderung der Route zu erhalten, nimmt Isa ihr Alltagshandy mit (das einzige, das sie hat).

### Annahmen

Isa hat keine Angst vor einer Verhaftung, denn bei ähnlichen Aktionen in der Vergangenheit, bei denen eine große Menschenmenge, die nicht der klassischen Antifa zuzuordnen ist, die Straßen blockierte, wurden sie von der Polizei lediglich gekesselt oder aus dem Weg geschleppt, bevor sie die Faschos umleiteten.
Sie glaubt nicht, dass mensch im Falle ihrer Verhaftung ihr Handy durchsuchen würde, weder legal noch illegal.
Sie macht sich auch keine Sorgen um die Standortdaten ihres Handys.

### Gegenmaßnahmen

Isa hat keine Gegenmaßnahmen gegen die Erfassung ihrer Standortdaten oder die Beschlagnahmung ihres Handys ergriffen.

### Analyse

Bei Massenaktionen kann die Polizei mit Hilfe von IMSI-Catchern feststellen,[^Massenaktionen-IMSI] wer an diesen Demos teilgenommen hat, um Profile zu erstellen.
Diese Standortdaten können verwendet werden, um Menschen wegen Ausschreitungen anzuklagen, selbst wenn die Anklagen nicht zu einer Verurteilung führen.

[^Massenaktionen-IMSI]: *Anm. d. Übersetzers*: Immer noch muss ich sagen, dass IMSI-Catcher sind (derzeit) nur selten in der BRD benutzt.

Wenn Isa verhaftet wird, was immer noch passieren kann, wenn die Blockaden zu klein sind oder sie zu den Pechvögeln gehört, die auf der Straße erwischt werden, kann ihr Handy durchsucht werden.
Dadurch erfährt die Polizei möglicherweise von ihrem sozialen Netzwerk oder den Aktivitäten ihrer engeren Freund:innen.
Dies kann ihre Freund:innen mehr gefährden als sie selbst.

### Empfehlungen

Auch wenn Isa nicht mit einer Verhaftung rechnet, sollte sie vorsichtiger mit ihrem Handy umgehen.
Sie könnte vereinbaren, sich mit Freunde:innen an einem festen Ort und zu einer festen Zeit vor der Demo zu treffen, so dass sie ihre Handys gar nicht erst mitnehmen müssen, oder wenn sie wirklich Informationen in Echtzeit haben wollen, sollte nur eine Person in ihrer Gruppe ein Handy mitbringen.
Ein vorsichtiger Umgang mit ihrem Handy kann ihre radikalen Freund:innen schützen, die sich möglicherweise militanter gegen die Faschos setzen.

Die Wahrscheinlichkeit, dass eines dieser Ereignisse eintritt, ist jedoch gering, und der wahrgenommene Nutzen der Mitnahme eines Handys ist hoch.
Das macht diesen Fall zu einem, in dem es für Isa „in Ordnung“ ist, ihr Handy mitzunehmen... bis es plötzlich nicht mehr sicher ist.

## Fall 7: Allgemeine Planung und Kommunikation

### Szenario

Ein Kollektiv organisiert legale Demos und verteilt Flyers, in denen für grüne und ökologische Alternativen zum derzeitigen Status quo geworben wird, wie z. B. die Umstellung auf vegane Ernährung, eine bessere Finanzierung der Fahrradinfrastruktur und eine geringere Abhängigkeit von Privatfahrzeugen.
Sie verwenden eine E-Mail-Liste, die auf einem Server gehostet wird, der von einigen lokalen Tech-Aktivist:innen bereitgestellt wird.

### Annahmen

Das Kollektiv geht davon aus, dass die Polizei generell an Aktivist:innen interessiert ist, dass aber das Kollektiv selbst nicht speziell ins Visier genommen wird.
Sie wissen, dass die lokalen Trolle gerne die „Hippie-Kommunisten“ belästigen.
Sie wissen auch, dass es andere, militantere grüne Kollektive in ihrer Region gibt und dass Mitglieder ihres Kollektivs in allen möglichen anderen Gruppen sein können.

### Gegenmaßnahmen

Das Kollektiv möchte Belästigungen vermeiden, daher halten sie ihre E-Mail-Liste privat und laden nur ein.
Sie wollen die Verfolgung durch große E-Mail-Anbieter vermeiden und hosten daher ihre E-Mails selbst.

### Analyse

E-Mail-Listen sind sehr beliebt, weil jede:r Zugang zu E-Mail hat, aber es gibt viele verschiedene Chat-Apps, und nicht jeder verwendet die gleichen, so dass Kollektive dazu neigen, weiterhin E-Mail-Listen zu verwenden.
Oft geben die Leute an, dass sie nicht genug Speicherplatz auf ihren Handys für weitere Apps haben.
Einige Mitglieder von Kollektiven verfügen über geringe technische Kenntnisse und wollen keine anderen Apps erlernen, so dass E-Mail manchmal unvermeidbar ist.

Ökoaktivist:innen weltweit, auch in den sogenannten westlichen Demokratien, werden gezielt überwacht, auch wenn sie nicht an direkten Aktionen teilnehmen.
Die E-Mail-Liste selbst zu hosten, mag die Überwachung durch Unternehmen verringern, aber es gibt immer eine Schwachstelle, wenn Daten angefordert werden.
Ein großer Provider könnte der Aufforderung nachkommen, ohne das Kollektiv zu benachrichtigen, und während die Techniker:innen, die den Server für das Kollektiv betreiben, sie wahrscheinlich ruhig informieren würden, selbst wenn sie eine Nachrichtensperre hätten, könnte die Polizei diese umgehen, indem sie sich direkt an die Hosting-Firma des Servers wendet und sie vorlädt.
Außerdem haben die Techniker:innen vielleicht nicht die technische Kompetenz eines großen E-Mail-Anbieters, um den Server sicher zu halten oder überhaupt zu bemerken, wenn er von Trollen oder dem Staat gehackt wird.

### Empfehlungen

Wenn der Platz auf dem Handy ein Problem ist, sollten die Aktivist:innen ein Backup von ihren Fotos und Videos machen dann sie löschen, um Platz zu schaffen.
Dies ist generell eine gute Praxis, um Daten zu sichern, falls das Handy verloren geht oder kaputt geht.

Das Kollektiv sollte idealerweise auf eine verschlüsselte Chat-App umsteigen, aber wenn sie weiterhin E-Mails verwenden, dann nur für die grundlegendsten Details wie Zeiten und Orte ihrer Aktivitäten.
Planungen, interne Debatten und wichtige Diskussionen sollten nicht per E-Mail ausgetauscht werden, da diese Informationen dem Staat große Einblicke in das Kollektiv geben können, die zur Störung genutzt werden können.

## Fall 8: Underground Raves

### Szenario

Ein Kollektiv plant underground Raves im Freien während der Coronavirus-Pandemie.
Sie bitten die Leute, Masken zu tragen, und halten dies für ausreichend sicher, was die Verbreitung des Virus angeht.
Die Polizei hat ein generelles Verbot von Massenversammlungen verhängt (außer bei der Arbeit und anderen Dingen, die die Kapitalmaschine am Laufen halten).

### Annahmen

Der Staat hat aktive Anstrengungen unternommen, um Massenversammlungen aufzulösen (natürlich nur der Arbeiterklasse und den Marginalisierte), aber er wird wahrscheinlich nicht rückwirkend nach Beweisen für diese Versammlungen suchen.
Die Polizei ist in der Lage, Telefonortungsdaten in Echtzeit zu sammeln, mit denen sie mehrere hundert Personen an einem abgelegenen Ort aufspüren kann.
Die Polizei hat Spitzel:innen, die auf solche Dinge achten, und manche Leute verpfeifen andere gerne, wenn sie von etwas hören, das ihnen nicht gefällt.

### Gegenmaßnahmen

Der Rave wird nicht in den sozialen Medien gepostet, und es wird darum gebeten, die Informationen nur auf sicherem Kanäle an weitere Kontakte weiterzuleiten.
In der Info werden die Leute gebeten, ihre Handys in den Flugmodus zu schalten, wenn sie sich dem Veranstaltungsort nähern.

### Analyse

Die Veranstaltung nicht zu veröffentlichen, ist ein offensichtlich richtiger Schritt, um zu verhindern, dass die Polizei von sich aus davon erfährt.
Die Aufforderung, die Informationen nur an vertrauenswürdige Kontakte auf sichere Kanäle weiterzugeben, ist ebenfalls ein guter Weg, um das Risiko zu verringern, aber es genügt eine Person, die eine verkürzte Nachricht mit nur dem Ort und der Uhrzeit weiterleitet, damit die Warnung verloren geht.
Selbst wenn das Kollektiv dies weiß, ist es ein Risiko, das es eingehen muss.

### Empfehlungen

Es gibt wenig, was das Kollektiv tun kann, um zu verhindern, dass Leute mit eingeschalteten Handys ankommen, und es gibt wenig, was sie tun können, um sicherzustellen, dass die Nachricht nur in den vertrauenswürdigen und sicherheitsbewussten Teilen des sozialen Netzes bleibt.
Dies ist ein schwieriges Problem in der Sicherheitskultur, da das Fehlen von OpSec bei einem Bruchteil der Teilnehmer:innen immer noch die ganze Gruppe zu Fall bringen kann, zumal der individuelle Nutzen eines eingeschalteten Handys hoch, das Risiko für den Einzelnen aber gering ist.
Die Organisator:innen, die den Rave organisieren und die Ausrüstung mitgebracht haben, müssen am ehesten mit Konsequenzen rechnen.
Wenn sich die Menge während einer Razzia zerstreut, werden sie wahrscheinlich keine Konsequenzen zu befürchten haben.
Das Beste, was das Kollektiv tun kann, ist zu versuchen, die Leute vor und während der Veranstaltung mit Schamgefühlen davon zu überzeugen, dass ihre Handlungen den Rave für alle ruinieren könnten.

## Fall 9: Umgang mit einem schwachen Glied

### Szenario

Eine Bezugsgruppe hat es auf Nazis abgesehen, die Menschen in ihrer Community belästigen.
Sie haben sich darauf geeinigt, zu ihren nächtlichen Aktionen keine Handys mitzubringen.
Felix, eines ihrer aktivsten Mitglieder, hält dies für übertrieben paranoid und weigert sich, sein Handy zu Hause zu lassen.

### Annahmen

Die Bezugsgruppe geht davon aus, dass der Staat die Standortdaten des Handys nutzen könnte, um ihre Aktivitäten zu untersuchen.
Sie gehen auch davon aus, dass Felix, der sein Handy mitbringt, sie alle in Gefahr bringt.

### Gegenmaßnahmen

Um zu verhindern, dass Felix sie gefährdet, haben sie ihre Aktivitäten gestoppt, bis sie eine Einigung mit Felix erzielen können.

### Analyse

Felixs Handlungen gefährden die Gruppe, und sie hat Recht, dass sie ihn nicht an ihren Aktionen teilnehmen lassen sollte.
Wenn die Gruppe ihre Aktionen vollständig einstellt, könnte ihrer Community noch mehr Schaden zugefügt werden, und das Risiko einer Verhaftung wegen der Handys könnte recht gering sein, je nachdem, wie die Polizei in der Region der Bezugsgruppe ermittelt.

### Empfehlungen

Die Bezugsgruppe könnte eine Untergruppe von Mitgliedern bilden, die sich darauf einigen, keine Handys zu Aktionen mitzubringen und ihre Arbeit fortzusetzen.
Parallel dazu könnten sie mit Felix arbeiten, um ihm klar zu machen, wie und warum das Mitbringen seines Handys ein unnötiges Risiko darstellt.
Sie könnten mit ihm besprechen, dass sein Verhalten ihnen Unbehagen bereitet und dass sein Verhalten nicht nur ihn selbst betrifft.
Die Gruppe könnte in der Lage sein, Genoss:innen mit ihm zu bleiben, aber sie müssen ihn vielleicht von geheimen Aktionen ausschließen, wenn er sich weigert, sein Handy zu Hause zu lassen.
