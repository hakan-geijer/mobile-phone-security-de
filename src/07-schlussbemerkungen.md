<!-- vim: set spelllang=de : -->

# Schlussbemerkungen

Technik ist weder gut noch schlecht---zumindest die meisten nicht.
Sie ist nicht von Natur aus befreiend oder unterdrückend.
Neue Technologien schaffen neue Gelegenheiten, während sie andere ausschließen.
Bei Handys ist das nicht anders.
Der Zugang zu sofortiger Kommunikation und umfangreichem Wissen in unserer Hosentasche ist unermesslich leistungsfähig, hat aber auch den Preis einer verstärkten Überwachung.

Du denkst vielleicht, dass der Staat dich nicht überwacht, aber wenn du in einer aktivistischen Bewegung aktiv bist---und sei es auch nur in geringem Umfang---dann ist er es mit Sicherheit.
Wenn du dich selbst schützt, kannst du deine Freund:innen, Familie oder Genoss:innen schützen, die in die Bewegung involviert sind.
Du denkst vielleicht, dass der Staat dein Handy hackt, um die wöchentlichen Plena deines Hausprojekts abzuhören, aber das ist sicherlich nicht der Fall.
Maximale Sicherheit zu jeder Zeit ist unerreichbar, und sie anzustreben ist anstrengend.

Nachdem du diese Zine gelesen hast, bist du vielleicht versucht zu sagen: „Aber sie werden mich auf jeden Fall aufspüren.“
Die Überzeugung, dass ein gewisses Maß an Sicherheit vor äußeren Bedrohungen unmöglich ist, wird als Sicherheitsnihilismus bezeichnet.
Menschen, die so denken, schlagen oft einen von zwei Wegen ein.
Sie können glauben, dass keine Gegenmaßnahmen funktionieren, also handeln sie weiter und treffen keine Vorsichtsmaßnahmen, was zu einer sich selbst erfüllenden Prophezeiung führt, die mit ihrer Verhaftung endet.
Oder sie glauben an die Vormachtstellung des Staates und werden durch Untätigkeit gelähmt.
Repression funktioniert nicht nur wegen der Peitsche, die uns trifft, oder des Gefängnisses, in das wir eingesperrt werden, sondern auch wegen der Angst vor diesen Strafen und unserer daraus resultierenden selbst auferlegten Untätigkeit.

Jede Maßnahme, die du ergreifst, kann dich schützen, und viele von ihnen sind so einfach, dass du sie sofort anwenden kannst.
Im einfachsten Fall kannst du eine Rasterfahndung vermeiden, indem du einfache verschlüsselte Chat-Apps verwendest und dein Handy bei Demos oder direkten Aktionen zu Hause lasst.
Jeder Schritt, den du darüber hinaus gehst, erfordert von deinen Gegner:innen mehr konzertierte Anstrengungen, wenn sie dich überwachen oder stören wollen.
Zeit und Ressourcen sind begrenzt, selbst bei den großen Geheimdiensten.
Menschen machen Fehler, und Computer gehen kaputt.
Deine Gegner:innen sind fehlbar, und du kannst die Menge der Daten, die sie erfassen können, und die Art der Erkenntnisse, die sie daraus gewinnen können, erheblich verringern.

Außerdem setzt der Staat nicht immer die theoretisch maximal möglichen Überwachungsmethoden ein.
Nur weil es dem Staat möglich ist, dein Handy zu hacken oder zu verfolgen, macht er das sicher nicht, um dich dabei zu erwischen, wie du nach der Sperrstunde durch die Parks läufst.
Selbst in Fällen, in denen der Staat ein Maximum an Überwachung anstrebt, kann es sein, dass er dies auf ungeschickte Weise tut.
Dein Bedrohungsmodell sollte die realistisch zu erwartende Reaktion deiner Gegner:innen berücksichtigen, da diese über deine Aktionen Bescheid wissen.

Informiere dich über die Arbeitsweise von Polizei, Faschos und anderen Gegner:innen in deinem Gebiet und entwickle ein Bedrohungsmodell für dich und deine Bezugsgruppe.
Diskutiere es ausführlich mit deinen Genoss:innen.
Beginne mit ein paar OpSec-Tips und verwandle sie in eine Sicherheitskultur.
Fördere ein gemeinsames Verständnis und Praktiken, die zu mehr Sicherheit gegen die Bedrohungen führen, mit denen du wahrscheinlich konfrontiert wirst.
Ergreife konkrete Maßnahmen, aber gehe pragmatisch vor.
Beginne langsam mit ein paar neuen Dingen, bis sie zur Normalität geworden sind, und baue dann von dort aus auf.
Ein Plan ist nur dann gut, wenn mensch ihn auch durchführt, und der Versuch, einer Gruppe viele große Veränderungen auf einmal aufzudrängen, ist in der Regel überwältigend und frustrierend.
Die meisten erfolgreichen Pläne werden schrittweise umgesetzt.

Hüte dich vor Mythen.
Aktivistische Räume sind voll davon, und bei der Sicherheit gibt es keine Ausnahme.
Frag mal „wie?“ und „warum?“, wenn jemand Behauptungen über Überwachung oder Gegenmaßnahmen aufstellt.
Stütze dein Bedrohungsmodell und deinen Sicherheitsplan auf überprüfbare Fakten---oder zumindest auf sehr wahrscheinliche Vermutungen mit entsprechenden Beweisen.

Nutze dieses Wissen, um dich zu schützen, während du die Welt umgestaltest.
